*** Settings ***
Library  Selenium2Library
Library  String

*** Variables ***
${URL}  https://10.0.0.84:8443/
#https://171.99.133.30:17443/
#https://10.0.0.84:8443/welcome
#https://bblwpsdev.sycapt.com:17443/
${HOME}   hhttps://10.0.0.84:8443/dashboard.action
${LOGINUSERNAME}  ROBOTGIRL
${LOGINPASSWORD}  P@ssw8rd

*** Keywords ***
Create Custom Option Chrome Webdriver
    [Documentation]  Create Custom Option Chrome Webdriver
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --window-size\=1920,1000     #1440,900
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    Create Webdriver    Chrome    chrome_options=${chrome_options}

Zoom of page
    [Arguments]  ${zoom}
    Execute javascript  document.body.style.zoom="${zoom}%"
    Sleep  1s

Go To GUI
    Create Custom Option Chrome Webdriver
    Go to    ${URL}
    Skip SSL
    Login to website

Go to Home
    Sleep   1s
    Go to    ${HOME}
    Sleep   1s

Scroll Page To Location
    [Arguments]  ${x_location}   ${y_location}
    Execute Javascript  window.scrollTo(${x_location},${y_location})

Login to website
    Input Text  name=username  ${LOGINUSERNAME}
    Input Text  name=password  ${LOGINPASSWORD}
    ## Wait Until Keyword Succeeds   [Timeout]   [Retry]   [Keyword]
    Wait Until Keyword Succeeds   10s  2s   Click Button  name=btnLogin   ##จะเรียกทุกๆ 2s จนครบ 10s
    Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Welcome to Wallet Payment Switching system.

## Click Button ##
Click Close Pop-up alert
    Sleep   0.5s
    Wait Until Keyword Succeeds   10s  2s   Click Element    xpath=//button[contains(@class, "ui-button ui-corner-all ui-widget") and contains(text(), "Close")]

Click Yes Pop-up Confirm
    Sleep   0.5s
    Wait Until Keyword Succeeds   10s  2s   Click Element    xpath=//button[contains(@class, "ui-button ui-corner-all ui-widget") and contains(text(), "Yes")]

Click No Pop-up Confirm
    Sleep   0.5s
    Wait Until Keyword Succeeds   10s  2s   Click Element    xpath=//button[contains(@class, "ui-button ui-corner-all ui-widget") and contains(text(), "No")]

Click Save Button on create page
    ## Wait Until Element Is Visible   [Element]   [Time out]
    Wait Until Element Is Visible   ${btn_save}   5s
    Click Element   ${btn_save}

Click Search Button
    ## Wait Until Element Is Visible   [Element]   [Time out]
    Wait Until Element Is Visible   ${btn_search}   10s
    Click Element   ${btn_search}

Click Delete Button
    ## Wait Until Element Is Visible   [Element]   [Time out]
    Wait Until Element Is Visible   ${btn_delete}  5s
    Click Element    ${btn_delete}

Click Delete Images
    ## Wait Until Element Is Visible   [Element]   [Time out]
    Wait Until Element Is Visible   ${btn_delete}  5s
    Sleep   1s
    Click Image    ${btn_delete}

Click Close Button on create page
    ## Wait Until Element Is Visible   [Element]   [Time out]
    Wait Until Element Is Visible   ${btn_close_create}   5s
    Click Element   ${btn_close_create}

Click Close Button on edit page
    ## Wait Until Element Is Visible   [Element]   [Time out]
    Wait Until Element Is Visible   ${btn_close_edit}   5s
    Click Element   ${btn_close_edit}

Click Reset Button on search page
  ## Wait Until Element Is Visible   [Element]   [Time out]
    Wait Until Element Is Visible   ${btn_reset_search}   10s
    Click Element   ${btn_reset_search}

Click Reset Button on create page
    ## Wait Until Element Is Visible   [Element]   [Time out]
    Wait Until Element Is Visible   ${btn_reset_create}   10s
    Click Element   ${btn_reset_create}

Click Reset Button on edit page
    ## Wait Until Element Is Visible   [Element]   [Time out]
    Wait Until Element Is Visible   ${btn_reset_edit}   10s
    Click Element   ${btn_reset_edit}

Click Save Button on Edit page
    ## Wait Until Element Is Visible   [Element]   [Time out]
    Wait Until Element Is Visible   ${btn_save_edit}   10s
    Click Element   ${btn_save_edit}

Confirm to Save
    Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to save this item ?
    sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
    Click Yes Pop-up Confirm

Confirm to Delete
    Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to delete this item ?
    sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
    Click Yes Pop-up Confirm

Confirm to Close
    Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to close this page ?
    sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
    Click Yes Pop-up Confirm

Confirm to Reset create
    Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to reset this item ?
    sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
    Click Yes Pop-up Confirm

Confirm to Reset create (reset this from)
    Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to reset this form ?
    sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
    Click Yes Pop-up Confirm

Confirm to Reset edit
    Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to reset this form ?
    sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
    Click Yes Pop-up Confirm

Confirm to Delete with display name
  [Arguments]   ${name}
    Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to delete ${name} ?
    sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
    Click Yes Pop-up Confirm

Confirm to Save Terminal
    Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to save this item ?
    sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
    Click Yes Pop-up Confirm Terminal

Click Yes Pop-up Confirm Terminal
    Sleep   0.5s
    Click Element    xpath=/html/body/div[5]/div[3]/div/button[1]

Skip SSL
    click element   xpath=//*[@id="details-button"]
    click link  //*[@id="proceed-link"]

## Display ##
Text Display alert message
    [Arguments]  ${alertmsg}
    Wait Until Keyword Succeeds   10s  2s   element text should be    xpath=//div[@class="ui-dialog-content ui-widget-content"]   ${alertmsg}
    Sleep   1.5s

Text Display Saved successfully
    Wait Until Page Contains   Saved successfully.   5s
    Sleep   1.5s

Text Display Data is not found
    Wait Until Page Contains  Data is not found.   5s