*** Settings ***
Documentation  I'm robot and I executing my source code.
Resource   ../Resources/BrowserLoginKeyword.robot
Suite Setup    Go To GUI
Test Setup   Go to Business Parameters
Test Teardown   Go to Home
Suite Teardown  Close Browser

*** Variables ***
${link_view}  /utility/otherutility/businesspara_e?cfgname=  #link view parameters: VATRATE
${btn_save}  //*[@id="saveBtn"]
${btn_close}  //*[@id="businesspara-form"]/table/tbody/tr[9]/td/input[2]

*** Test Cases ***
TC_01 Update Descr and Value in parameter1: success
  [Tags]    success
  Click to Open Parameters   PAGESIZE
  Update Descr   MAXIMUM LIST DATA SHOW ON HTML TABLE
  Update Value   50
  Click Save Button
  Confirm to Save
  Text Display Saved successfully.
  Click to Open Parameters   PAGESIZE
  Checking parameter is valid and Close Page   MAXIMUM LIST DATA SHOW ON HTML TABLE    50

TC_02 Update Descr and Value in parameter2: success
  [Tags]    success
  Click to Open Parameters   VATRATE
  Update Descr   VAT PERCENT (%)
  Update Value   7.00
  Click Save Button
  Confirm to Save
  Text Display Saved successfully.
  Click to Open Parameters   VATRATE
  Checking parameter is valid and Close Page    VAT PERCENT (%)    7.00

TC_03 Update Value is character type in parameter type = Int: parameter1
  [Tags]    failure
  Click to Open Parameters   PAGESIZE
  Update Value   tEst
  Click Save Button
  Text Display alert message    Value can contain only numbers.
  Click Close Pop-up alert

TC_04 Update Value is character type in parameter type = Percentage: parameter2
  [Tags]    failure
  Click to Open Parameters   VATRATE
  Update Value   tEst
  Click Save Button
  Text Display alert message    Value is in an invalid format.
  Click Close Pop-up alert

TC_05 Update Value = 0 in parameter: parameter1
  [Tags]    failure
  Click to Open Parameters   PAGESIZE
  Update Value   0
  Click Save Button
  Text Display alert message    Value is in an invalid format.
  Click Close Pop-up alert

TC_06 Update value more than 100 in parameter type = Percentage: parameter2
  [Tags]    failure
  Click to Open Parameters   VATRATE
  Update Value   101
  Click Save Button
  Text Display alert message    Value is in an invalid format.
  Click Close Pop-up alert

TC_07 Update value is empty in parameter name: parameter1
  [Tags]    failure
  Click to Open Parameters   PAGESIZE
  Clear Value
  Click Save Button
  Text Display alert message    Value must be specified
  Click Close Pop-up alert

*** Keywords ***
Go to Business Parameters
  Page Should Contain Link  link=Administration
  Click Link  link=Administration
  Click Link  link=Business Parameters
  Page Should Contain  Business Parameters

Click to Open Parameters
  [Arguments]  ${param_name}
  Scroll Page To Location    200    200
  Wait Until Keyword Succeeds   10s  2s   Click Link  ${link_view}${param_name}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  ${param_name}

Update Descr
  [Arguments]  ${descr}
  Input Text  name=cfgdescript  ${descr}

Update Value
  [Arguments]  ${value}
  Input Text  name=bparaval  ${value}

Update Value is character
  Input Text  name=bparaval  TEST

Update Value is 0
  Input Text  name=bparaval  0

Update Value is 101
  Input Text  name=bparaval  101

Update Value is random
  Input Text  name=bparaval  ${number}

Clear Value
  Clear Element Text  xpath=//*[@id="bparaval"]

Click Save Button
  Wait Until Element Is Visible   ${btn_save}   5s
  Click Element  ${btn_save}

Confirm to Save
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to save this item?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm

Click Close Button
  Wait Until Element Is Visible   ${btn_close}   5s
  Click Element  ${btn_close}

Confirm to Close
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to close this page ?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm
  Wait Until Page Contains  Business Parameters

Text Display Saved successfully.
  Wait Until Page Contains   Saved successfully.   10s

Checking parameter is valid and Close Page
  [Arguments]  ${descr}   ${value}
  Textarea Value Should Be   name=cfgdescript   ${descr}
  Textfield Value Should Be   name=bparaval   ${value}
  Click Close Button
  Confirm to Close

Random 1000 to 9999
  [Documentation]  Random number: ${number}
  ${firstdigit}    Generate random string  1   123456789
  ${random_number}    Generate random string    3    0123456789
  ${number}   Set Variable   ${firstdigit}${random_number}
  Set Test Variable   ${number}