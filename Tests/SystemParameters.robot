*** Settings ***
Documentation  I'm robot and I executing my source code.
Resource   ../Resources/BrowserLoginKeyword.robot
Suite Setup    Go To GUI
Test Setup   Go to System Parameters
Test Teardown   Go to Home
Suite Teardown  Close Browser

*** Variables ***
${btn_view}  //*[@id="result"]/tbody/tr[2]/td[8]/a  #view parameters: GRPNAMEMINLE
${btn_save}  //*[@id="formprfsparam"]/table[2]/tbody/tr[8]/td/input[1]
${btn_close}  //*[@id="formprfsparam"]/table[2]/tbody/tr[8]/td/input[3]
${parameter_1}  GRPNAMEMINLE
${value_1}  5

*** Test Cases ***
TC_01 Update Descr and Value in parameter: ${parameter_1} success
  [Tags]    success
  Click to Open Parameters
  Update Descr
  Update Value
  Click Save Button
  Confirm to Save
  Text Display Saved successfully.
  Click to Open Parameters
  Checking parameter is valid and Close Page

TC_02 Update Value is character type in parameter: ${parameter_1}
  [Tags]    failure
  Click to Open Parameters
  Update Value is character     #cannot input characters in this field
  Click Save Button
  Text Display alert message    Value must be specified.
  Click Close Pop-up alert

TC_03 Update Value = 0 in parameter: ${parameter_1}
  [Tags]    failure
  Click to Open Parameters
  Update Value is 0
  Click Save Button
  Text Display alert message    Value is in an invalid format.
  Click Close Pop-up alert

TC_04 Update value 1000 - 9999 in parameter name: ${parameter_1}
  [Tags]    failure
  Click to Open Parameters
  random 1000 to 9999
  Update Value is random
  Click Save Button
  Text Display alert message    Value is in an invalid format.
  Click Close Pop-up alert

TC_05 Update value is empty in parameter name: ${parameter_1}
  [Tags]    failure
  Click to Open Parameters
  Clear Value
  Click Save Button
  Text Display alert message    Value must be specified.
  Click Close Pop-up alert

*** Keywords ***
Go to System Parameters
  Page Should Contain Link  link=Administration
  Click Link  link=Administration
  Click Link  link=System Parameters
  Page Should Contain  System Parameters

Click to Open Parameters
  Wait Until Keyword Succeeds   10s  2s   Click Element  ${btn_view}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  ${parameter_1}

Update Descr
  Input Text  name=systemparam.cfgdescript  GROUP NAME LENGTH IS MINIMUM ${value_1} CHARS

Update Value
  Input Text  name=systemparam.cfgval  ${value_1}

Update Value is character
  Input Text  name=systemparam.cfgval  TEST

Update Value is 0
  Input Text  name=systemparam.cfgval  0

Update Value is random
  Input Text  name=systemparam.cfgval  ${number}

Clear Value
  Clear Element Text  xpath=//input[@id='cfgval']

Click Save Button
  Wait Until Element Is Visible   ${btn_save}   5s
  Click Element  ${btn_save}

Confirm to Save
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to save this item ?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm

Click Close Button
  Wait Until Element Is Visible   ${btn_close}   5s
  Click Element  ${btn_close}

Confirm to Close
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to close this page ?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm
  Wait Until Page Contains  System Parameters

Text Display Saved successfully.
  Wait Until Page Contains   Saved successfully.   5s

Checking parameter is valid and Close Page
  Textarea Value Should Be   name=systemparam.cfgdescript   GROUP NAME LENGTH IS MINIMUM ${value_1} CHARS
  Textfield Value Should Be   name=systemparam.cfgval   ${value_1}
  Click Close Button
  Confirm to Close

Random 1000 to 9999
  [Documentation]  Random number: ${number}
  ${firstdigit}    Generate random string  1   123456789
  ${random_number}    Generate random string    3    0123456789
  ${number}   Set Variable   ${firstdigit}${random_number}
  Set Test Variable   ${number}