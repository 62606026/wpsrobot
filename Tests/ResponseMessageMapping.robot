*** Settings ***
Documentation  I'm robot and I executing my source code.
Resource   ../Resources/BrowserLoginKeyword.robot
Suite Setup    Go To GUI
Test Setup   Go to Response Message Mapping
Test Teardown   Go to Home
Suite Teardown  Close Browser

*** Variables ***
${btn_search}  //*[@id="btnSubmit"]
${btn_reset_search}   //*[@id="search-response"]/table[2]/tbody/tr[3]/td/input[2]
${btn_view}  //*[@id="result"]/tbody/tr[1]/td[7]/a/img
${btn_save}  //*[@id="btnSubmit"]
${btn_close_create}   //*[@id="res-form"]/table[4]/tbody/tr/td[2]/div/input[3]
${btn_reset_create}   //*[@id="res-form"]/table[4]/tbody/tr/td[2]/div/input[2]
${btn_delete}  //*[@id="btnDelete"]
${btn_save_edit}   //*[@id="btnSubmit"]
${btn_close_edit}   //*[@id="res-form"]/table[4]/tbody/tr/td[2]/div/input[3]
${btn_reset_edit}   //*[@id="res-form"]/table[4]/tbody/tr/td[2]/div/input[2]

*** Test Cases ***
TC_01 Create: one rows successfully.
  [Tags]   success
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Input resp msg mapp profile  ${respmsgmapp}
  Input host resp msg row0  ${hostrespmsg_0}
  Input return resp msg row0   ${returnrespmsg_0}
  Select applied to row  0   -1
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search response message mapping  ${respmsgmapp}
  Click Search Button
  Click to Open response message mapping  ${respmsgmapp}
  Checking Value
  Click Close Button on edit page
  Confirm to close
  #Delete data that create test
    Search response message mapping  ${respmsgmapp}
    Click Search Button
    Click to Open response message mapping  ${respmsgmapp}
    Click Delete Button
    Confirm to Delete with display name   ${respmsgmapp}

TC_02 Create: more than one rows successfully.
  [Tags]   success
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Input resp msg mapp profile  ${respmsgmapp}
  Input host resp msg row0  ${hostrespmsg_0}
  Input return resp msg row0   ${returnrespmsg_0}
  Select applied to row  0   -1
  Generate Test host resp msg row1 len=   15
  Generate Test return resp msg row1 len=  15
  Add row
  Input host resp msg row1  ${hostrespmsg_1}
  Input return resp msg row1   ${returnrespmsg_1}
  Select applied to row  1   1
  Generate Test host resp msg row2 len=   5
  Generate Test return resp msg row2 len=  5
  Add row
  Input host resp msg row2  ${hostrespmsg_2}
  Input return resp msg row2   ${returnrespmsg_2}
  Select applied to row  2   2
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search response message mapping  ${respmsgmapp}
  Click Search Button
  Click to Open response message mapping  ${respmsgmapp}
  Checking value more than one row
  Click Close Button on edit page
  Confirm to close
  #Delete data that create test
    Search response message mapping  ${respmsgmapp}
    Click Search Button
    Click to Open response message mapping  ${respmsgmapp}
    Click Delete Button
    Confirm to Delete with display name   ${respmsgmapp}

TC_03 Create: Response Message Mapping Profile can contain only English letters and numbers.
  [Tags]   failure
  Click Create
  Input resp msg mapp profile   ชื่อภาษาไทย
  Click Save Button on create page
  Text Display alert message   Response Message Mapping Profile can contain only English letters and numbers.
  Click Close Pop-up alert

TC_04 Create: Response Message Mapping Profile must be specified.
  [Tags]   failure
  Click Create
  Input resp msg mapp profile   \
  Click Save Button on create page
  Text Display alert message   Response Message Mapping Profile must be specified.
  Click Close Pop-up alert

TC_05 Create: Response Message Mapping Profile [xxxx] already exists.
  [Tags]   failure
  Search response message mapping  \
  Click Search Button
  Get response message mapping name from search result
  Click Create
  Input resp msg mapp profile   KINGPOWER
  Click Save Button on create page
  Text Display alert message   Response Message Mapping Profile [KINGPOWER] already exists.
  Click Close Pop-up alert

TC_06 Create: Host Resp. message is in an invalid format
  [Tags]   failure
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Input resp msg mapp profile   ${respmsgmapp}
  Input host resp msg row0  โฮตท์เรสปอนส
  Click Save Button on create page
  Text Display alert message   Host Resp. message is in an invalid format
  Click Close Pop-up alert

TC_07 Create: Host Resp. message must be specified.
  [Tags]   failure
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Input resp msg mapp profile   ${respmsgmapp}
  Input host resp msg row0  \
  Click Save Button on create page
  Text Display alert message   Host Resp.Message must be specified.
  Click Close Pop-up alert

TC_08 Create: Host Resp.Message : xxxx already exists.
  [Tags]   failure
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Input resp msg mapp profile   ${respmsgmapp}
  Input host resp msg row0  ${hostrespmsg_0}
  Input return resp msg row0  ${returnrespmsg_0}
  Add row
  Input host resp msg row1  ${hostrespmsg_0}
  Click Save Button on create page
  Text Display host resp msg already exists  ${hostrespmsg_0}
  Click Close Pop-up alert

TC_09 Create: Return Resp. message is in an invalid format.
  [Tags]   failure
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Input resp msg mapp profile   ${respmsgmapp}
  Input host resp msg row0  ${hostrespmsg_0}
  Input return resp msg row0  รีเทรินแมวเซสทูไคลแอ้น
  Click Save Button on create page
  Text Display alert message   Return Resp. message is in an invalid format
  Click Close Pop-up alert

TC_10 Create: Return Resp. message must be specified.
  [Tags]   failure
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Input resp msg mapp profile   ${respmsgmapp}
  Input host resp msg row0  ${hostrespmsg_0}
  Input return resp msg row0  \
  Click Save Button on create page
  Text Display alert message   Return Resp. message must be specified.
  Click Close Pop-up alert

TC_11 Create: Add new row but not specify data
  [Tags]   failure
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Input resp msg mapp profile   ${respmsgmapp}
  Input host resp msg row0  ${hostrespmsg_0}
  Input return resp msg row0  ${returnrespmsg_0}
  Add row
  Click Save Button on create page
  Text Display alert message   Host Resp.Message must be specified.
  Click Close Pop-up alert

TC_12 Create: Test Remove row, Create and Delete
  [Tags]   success
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Input resp msg mapp profile  ${respmsgmapp}
  Input host resp msg row0  ${hostrespmsg_0}
  Input return resp msg row0   ${returnrespmsg_0}
  Select applied to row  0   -1
  Add row
  Remove row
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search response message mapping  ${respmsgmapp}
  Click Search Button
  Click to Open response message mapping  ${respmsgmapp}
  Checking Value
  Click Delete Button
  Confirm to Delete with display name   ${respmsgmapp}

TC_13 Reset button on create page
  [Tags]   success
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Input resp msg mapp profile  ${respmsgmapp}
  Input host resp msg row0  ${hostrespmsg_0}
  Input return resp msg row0   ${returnrespmsg_0}
  Select applied to row  0   -1
  Click Reset Button on create page
  Confirm to Reset create (reset this from)
  Checking reset create

TC_14 Close button on create page
  [Tags]   success
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Click Close Button on create page
  Confirm to close
  Display main menu page

TC_15 Create and Update add new rows: successfully.
  [Tags]   success
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Input resp msg mapp profile  ${respmsgmapp}
  Input host resp msg row0  ${hostrespmsg_0}
  Input return resp msg row0   ${returnrespmsg_0}
  Select applied to row  0   -1
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search response message mapping  ${respmsgmapp}
  Click Search Button
  Click to Open response message mapping  ${respmsgmapp}
  Checking value
  Generate Test host resp msg row1 len=   15
  Generate Test return resp msg row1 len=  15
  Add row
  Input host resp msg row1  ${hostrespmsg_1}
  Input return resp msg row1   ${returnrespmsg_1}
  Select applied to row  1   1
  Generate Test host resp msg row2 len=   5
  Generate Test return resp msg row2 len=  5
  Add row
  Input host resp msg row2  ${hostrespmsg_2}
  Input return resp msg row2   ${returnrespmsg_2}
  Select applied to row  2   2
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search response message mapping  ${respmsgmapp}
  Click Search Button
  Click to Open response message mapping  ${respmsgmapp}
  Checking value more than one row
  Click Close Button on edit page
  Confirm to close
  #Delete data that create test
    Search response message mapping  ${respmsgmapp}
    Click Search Button
    Click to Open response message mapping  ${respmsgmapp}
    Click Delete Button
    Confirm to Delete with display name   ${respmsgmapp}

TC_16 Reset button on edit page and Save without change
  [Tags]   success
  Generate Test resp msg mapping name len=   35
  Generate Test host resp msg row0 len=   46
  Generate Test return resp msg row0 len=  62
  Click Create
  Input resp msg mapp profile  ${respmsgmapp}
  Input host resp msg row0  ${hostrespmsg_0}
  Input return resp msg row0   ${returnrespmsg_0}
  Select applied to row  0   -1
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search response message mapping  ${respmsgmapp}
  Click Search Button
  Click to Open response message mapping  ${respmsgmapp}
  Checking value
  Generate Test host resp msg row1 len=   15
  Generate Test return resp msg row1 len=  15
  Add row
  Input host resp msg row1  ${hostrespmsg_1}
  Input return resp msg row1   ${returnrespmsg_1}
  Select applied to row  1   -1
  Click Reset Button on edit page
  Confirm to Reset edit
  Click Save Button on Edit page
  Confirm to Save
  Text Display Saved successfully
  Search response message mapping  ${respmsgmapp}
  Click Search Button
  Click to Open response message mapping  ${respmsgmapp}
  Checking value
  Click Close Button On edit Page
  Confirm to close
  #Delete data that create test
    Search response message mapping  ${respmsgmapp}
    Click Search Button
    Click to Open response message mapping  ${respmsgmapp}
    Click Delete Button
    Confirm to Delete with display name   ${respmsgmapp}

TC_17 Create and Update on existing row: Successfully
  [Tags]   success
  Generate Test resp msg mapping name len=   5
  Generate Test host resp msg row0 len=   10
  Generate Test return resp msg row0 len=  10
  Click Create
  Input resp msg mapp profile  ${respmsgmapp}
  Input host resp msg row0  ${hostrespmsg_0}
  Input return resp msg row0   ${returnrespmsg_0}
  Select applied to row  0   -1
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search response message mapping  ${respmsgmapp}
  Click Search Button
  Click to Open response message mapping  ${respmsgmapp}
  Checking value
  Input resp msg mapp profile  ${respmsgmapp}edit
  Input host resp msg row0  ${hostrespmsg_0}_edit
  Input return resp msg row0   ${returnrespmsg_0}_edit
  Select applied to row  0   1
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search response message mapping  ${respmsgmapp}
  Click Search Button
  Click to Open response message mapping  ${respmsgmapp}
  Checking value
  Click Close Button on edit page
  Confirm to close
  #Delete data that create test
    Search response message mapping  ${respmsgmapp}
    Click Search Button
    Click to Open response message mapping  ${respmsgmapp}
    Click Delete Button
    Confirm to Delete with display name   ${respmsgmapp}

TC_18 Reset button on search page
  [Tags]   success
  Search response message mapping  Test
  Select search format  2
  Click Reset Button on search page
  Checking reset search

*** Keywords ***
Go to Response Message Mapping
  Page Should Contain Link  link=Main Data
  Click Link  link=Main Data
  Click Link  link=Response Message Mapping

Click Create
  Page Should Contain Link  link=Create
  Click Link  link=Create
  Page Should Contain  Response Message Mapping Profile

Display main menu page
  Page Should Contain  Search
  Page Should Contain  Response Message Mapping Profile
  Page Should Contain  Format

## InputData ##
Input resp msg mapp profile
  [Arguments]  ${respmsgmapp}
  Input Text  name=model.resname   ${respmsgmapp}
  Set Test Variable   ${respmsgmapp}

Input host resp msg row0
  [Arguments]  ${hostrespmsg_0}
  Input Text  name=model.respmsgmaps[0].hostrespmsg   ${hostrespmsg_0}
  Set Test Variable   ${hostrespmsg_0}

Input return resp msg row0
  [Arguments]  ${returnrespmsg_0}
  Input Text  name=model.respmsgmaps[0].returnrespmsg   ${returnrespmsg_0}
  Set Test Variable   ${returnrespmsg_0}

Input host resp msg row1
  [Arguments]  ${hostrespmsg_1}
  Input Text  name=model.respmsgmaps[1].hostrespmsg   ${hostrespmsg_1}
  Set Test Variable   ${hostrespmsg_1}

Input return resp msg row1
  [Arguments]  ${returnrespmsg_1}
  Input Text  name=model.respmsgmaps[1].returnrespmsg   ${returnrespmsg_1}
  Set Test Variable   ${returnrespmsg_1}

Input host resp msg row2
  [Arguments]  ${hostrespmsg_2}
  Input Text  name=model.respmsgmaps[2].hostrespmsg   ${hostrespmsg_2}
  Set Test Variable   ${hostrespmsg_2}

Input return resp msg row2
  [Arguments]  ${returnrespmsg_2}
  Input Text  name=model.respmsgmaps[2].returnrespmsg   ${returnrespmsg_2}
  Set Test Variable   ${returnrespmsg_2}

Select applied to row
  [Arguments]  ${row}   ${wallet}
  Select Radio Button  model.respmsgmaps[${row}].wallethostid   ${wallet}
  Set Test Variable   ${row}
  Set Test Variable   ${wallet}

Select search format
  [Arguments]    ${format}
  Click Element   xpath=//*[@id="rptFormat"]/option[${format}]

## Click Button ##
Add row
  Click Element   //*[@id="res-form"]/div/a

Remove row
  Click Element   //*[@id="respmsgmapslists"]/li[2]/table/tbody/tr/td[1]/a/img

Click to Open response message mapping
  [Arguments]    ${respmsgmapp}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain    ${respmsgmapp}
  Click Image    ${btn_view}
  Wait Until Keyword Succeeds    10s    2s   Page Should Contain  Response Message Mapping Profile

## Display ##
Text Display host resp msg already exists
  [Arguments]   ${hostrespmsg}
  Wait Until Page Contains   Host Resp.Message [${hostrespmsg}] already exists.   5s
  Sleep   2s

Search response message mapping
  [Arguments]    ${respmsgmapp}
  Input Text  name=model.resname  ${respmsgmapp}

## Checking ##
Checking value
  Textfield Value Should Be   name=model.resname   ${respmsgmapp}
  Textfield Value Should Be   name=model.respmsgmaps[0].hostrespmsg   ${hostrespmsg_0}
  Textfield Value Should Be   name=model.respmsgmaps[0].returnrespmsg   ${returnrespmsg_0}
  Radio Button Should Be Set To    model.respmsgmaps[0].wallethostid   ${wallet}

Checking value more than one row
  Textfield Value Should Be   name=model.resname   ${respmsgmapp}
  Textfield Value Should Be   name=model.respmsgmaps[0].hostrespmsg   ${hostrespmsg_0}
  Textfield Value Should Be   name=model.respmsgmaps[0].returnrespmsg   ${returnrespmsg_0}
  Radio Button Should Be Set To    model.respmsgmaps[0].wallethostid   -1
  Textfield Value Should Be   name=model.respmsgmaps[1].hostrespmsg   ${hostrespmsg_1}
  Textfield Value Should Be   name=model.respmsgmaps[1].returnrespmsg   ${returnrespmsg_1}
  Radio Button Should Be Set To    model.respmsgmaps[1].wallethostid   1
  Textfield Value Should Be   name=model.respmsgmaps[2].hostrespmsg   ${hostrespmsg_2}
  Textfield Value Should Be   name=model.respmsgmaps[2].returnrespmsg   ${returnrespmsg_2}
  Radio Button Should Be Set To    model.respmsgmaps[2].wallethostid   2

Checking reset create
  Textfield Value Should Be   name=model.resname   \
  Textfield Value Should Be   name=model.respmsgmaps[0].hostrespmsg   \
  Textfield Value Should Be   name=model.respmsgmaps[0].returnrespmsg   \
  Radio Button Should Be Set To    model.respmsgmaps[0].wallethostid   -1

Checking reset search
  Textfield Value Should Be   id=resname   \
  List Selection Should Be   id=rptFormat   vh

## Generating ##
Generate Test resp msg mapping name len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890
    ${respmsgmapp}   Set Variable   TestRespMsgMapp${random_id}
    Set Test Variable   ${respmsgmapp}

Generate Test host resp msg row0 len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=-)(*&^%$#@!_'\/?><.,;:[]}{
    ${hostrespmsg_0}   Set Variable   H_${random_id}
    Set Test Variable   ${hostrespmsg_0}

Generate Test return resp msg row0 len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=-)(*&^%$#@!+_'\/?><.,;:[]}{
    ${returnrespmsg_0}   Set Variable   C_${random_id}
    Set Test Variable   ${returnrespmsg_0}

Generate Test host resp msg row1 len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=-)(*&^%$#@!+_'\/?><.,;:[]}{
    ${hostrespmsg_1}   Set Variable   H_${random_id}
    Set Test Variable   ${hostrespmsg_1}


Generate Test return resp msg row1 len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=-)(*&^%$#@!+_'\/?><.,;:[]}{
    ${returnrespmsg_1}   Set Variable   C_${random_id}
    Set Test Variable   ${returnrespmsg_1}

Generate Test host resp msg row2 len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=-)(*&^%$#@!+_'\/?><.,;:[]}{
    ${hostrespmsg_2}   Set Variable   H_${random_id}
    Set Test Variable   ${hostrespmsg_2}


Generate Test return resp msg row2 len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=-)(*&^%$#@!+_'\/?><.,;:[]}{
    ${returnrespmsg_2}   Set Variable   C_${random_id}
    Set Test Variable   ${returnrespmsg_2}

## Getting Value ##
Get response message mapping name from search result
  Sleep   1s
  ${name}   Get Value   xpath=//*[@id="result"]/tbody/tr[4]/td[2]
  Set Test Variable   ${name}