*** Settings ***
Documentation  I'm robot and I executing my source code.
Resource   ../Resources/BrowserLoginKeyword.robot
Suite Setup    Go To GUI
Test Setup   Go to notification channel
Test Teardown   Go to Home
Suite Teardown  Close Browser

*** Variables ***
${btn_save}  //*[@id="save"]
${btn_delete}  //*[@id="delete"]
${btn_search}  //*[@id="search-form"]/table/tbody/tr[3]/td/input[1]
${btn_view}  //*[@id="result"]/tbody/tr[1]/td[7]/a/img
${btn_save_edit}   //*[@id="create-form"]/table/tbody/tr[12]/td[2]/input[1]
${btn_close_edit}   //*[@id="create-form"]/table/tbody/tr[12]/td[2]/input[3]
${btn_close_create}   //*[@id="create-form"]/table/tbody/tr[8]/td[2]/input[3]
${btn_reset_edit}   //*[@id="create-form"]/table/tbody/tr[12]/td[2]/input[2]
${btn_reset_create}   //*[@id="create-form"]/table/tbody/tr[8]/td[2]/input[2]
${btn_reset_search}   //*[@id="search-form"]/table/tbody/tr[3]/td/input[2]

*** Test Cases ***
TC_01 Create: successfully
  [Tags]   success
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  'PUKTESt123012345678
  Input Text notif trans. code  'PUKTESt105481215454
  Input Text ws notif url    http://10.0.0.109:9991/trade/wps/v1/notify/transType/@transType@/transCode/@transCode@/clientId/@clientId@/providerId/@providerId@/
  Input Text notif client ID  notifid1
  Input Text notif username  'User.n@m3_n0tif
  Input Text notif password  'notif.P@ss09_n0tif
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search ws notif code  ${notif_code}
  #Search Like ws notif code  ${notif_code}
  Click Search Button
  Click to Open notif channel  ${notif_code}
  Checking value   ${notif_code}
  Click Close Button on edit page
  Click Yes Pop-up Confirm
  Display main screen Notification Channel

TC_02 Create: ws notif code already exists
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  IPAY
  Click Save Button on create page
  Text Display alert message   Web Service Notification Code[IPAY] already exists.
  Click Close Pop-up alert

TC_03 Create: ws notif code with special characters
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  Special_test
  Click Save Button on create page
  Text Display alert message   Web Service Notification Code can contain only English letters and numbers.
  Click Close Pop-up alert

TC_04 Create: ws notif code must be specified
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Click Save Button on create page
  Text Display alert message   Web Service Notification Code must be specified.
  Click Close Pop-up alert

TC_05 Create: notif trans. type must be specified
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Click Save Button on create page
  Text Display alert message   Notification Trans. Type must be specified.
  Click Close Pop-up alert

TC_06 Create: notif trans. type with thai characters
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  ทอสอบภาษาไทย
  Click Save Button on create page
  Text Display alert message   Notification Trans. Type can contain only English letters, numbers, and special characters.
  Click Close Pop-up alert

TC_07 Create: notif trans. code with thai characters
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  notif_Sale
  Input Text notif trans. code  ภาษาไทยใช้ทดสอบ
  Click Save Button on create page
  Text Display alert message   Notification Trans. Code can contain only English letters, numbers, and special characters.
  Click Close Pop-up alert

TC_08 Create: notif trans. code must be specified
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  notif_Sale
  Click Save Button on create page
  Text Display alert message   Notification Trans. Code must be specified.
  Click Close Pop-up alert

TC_09 Create: ws notif url must be specified
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  notif_Sale
  Input Text notif trans. code  TranS-Code01
  Click Save Button on create page
  Text Display alert message   Web Service Notification URL must be specified.
  Click Close Pop-up alert

TC_10 Create: notif Client ID must be specified
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  notif_Sale
  Input Text notif trans. code  TranS-Code01
  Input Text ws notif url  '<script>alert('https://www.google.co.th')</script>;
  Click Save Button on create page
  Text Display alert message   Notification Client ID must be specified.
  Click Close Pop-up alert

TC_11 Create: notif Client ID with special characters
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  notif_Sale
  Input Text notif trans. code  TranS-Code01
  Input Text ws notif url  '<script>alert('https://www.google.co.th')</script>;
  Input Text notif client ID  Client_ID-01
  Click Save Button on create page
  Text Display alert message   Notification Client ID can contain only English letters and numbers.
  Click Close Pop-up alert

TC_12 Create: notif username with Thai characters
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  notif_Sale
  Input Text notif trans. code  TranS-Code01
  Input Text ws notif url  '<script>alert('https://www.google.co.th')</script>;
  Input Text notif client ID  ClientID01
  Input Text notif username  Uกernamย
  Click Save Button on create page
  Text Display alert message   Notification UserName can contain only English letters, numbers, and special characters.
  Click Close Pop-up alert

TC_13 Create: notif username must be specified
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  notif_Sale
  Input Text notif trans. code  TranS-Code01
  Input Text ws notif url  '<script>alert('https://www.google.co.th')</script>;
  Input Text notif client ID  ClientID01
  Click Save Button on create page
  Text Display alert message   Notification UserName must be specified.
  Click Close Pop-up alert

TC_14 Create: notif password must be specified
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  notif_Sale
  Input Text notif trans. code  TranS-Code01
  Input Text ws notif url  '<script>alert('https://www.google.co.th')</script>;
  Input Text notif client ID  ClientID01
  Input Text notif username  User.n@m3_n0tif
  Click Save Button on create page
  Text Display alert message   Notification Password must be specified.
  Click Close Pop-up alert

TC_15 Create: notif password with Thai characters
  [Tags]    failure
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  notif_Sale
  Input Text notif trans. code  TranS-Code01
  Input Text ws notif url  '<script>alert('https://www.google.co.th')</script>;
  Input Text notif client ID  ClientID01
  Input Text notif username  User.n@m3_n0tif
  Input Text notif password  P@สส.n@m3_n0tif
  Click Save Button on create page
  Text Display alert message   Notification Password can contain only English letters, numbers, and special characters.
  Click Close Pop-up alert

TC_16 Delete: successfully
  [Tags]    success
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  'PUKTESt123012345678
  Input Text notif trans. code  'PUKTESt105481215454
  Input Text ws notif url    '<script>alert('https://www.google.co.th')</script>;<script>alert('https://www.google.co.th')</script>;<script>alert('https://www.google.co.th')</script>;<script>alert('https://www.google.co.th')</scr
  Input Text notif client ID  notifid1
  Input Text notif username  'User.n@m3_n0tif
  Input Text notif password  'notif.P@ss09_n0tif
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search ws notif code  ${notif_code}
  #Search Like ws notif code  ${notif_code}
  Click Search Button
  Click to Open notif channel  ${notif_code}
  Checking value   ${notif_code}
  Click Delete Button on edit page
  Confirm to delete
  Text Display Deleted successfully  ${notif_code}
  Display main screen Notification Channel
  Search ws notif code  ${notif_code}
  #Search Like ws notif code  ${notif_code}
  Click Search Button
  Text Display Data is not found

TC_17 Reset button on search page
  [Tags]    success
  Generate WS Notification Code
  Search ws notif code  ${notif_code}
  Select search format  2
  Click Reset Button on search page
  Checking reset search

TC_18 Reset button on create page
  [Tags]    success
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  'PUKTESt123012345678
  Input Text notif trans. code  'PUKTESt105481215454
  Input Text ws notif url    http://10.0.0.109:9991/trade/wps/v1/notify/transType/@transType@/transCode/@transCode@/clientId/@clientId@/providerId/@providerId@/
  Input Text notif client ID  notifid1
  Input Text notif username  'User.n@m3_n0tif
  Input Text notif password  'notif.P@ss09_n0tif
  Click Reset Button on create page
  Confirm to Reset create
  Checking reset create   ${notif_code}

TC_19 Reset button on edit page and saved without change
  [Tags]    success
  Generate WS Notification Code
  Click Create
  Input Text ws notif code  ${notif_code}
  Input Text notif trans. type  'PUKTESt123012345678
  Input Text notif trans. code  'PUKTESt105481215454
  Input Text ws notif url    http://10.0.0.109:9991/trade/wps/v1/notify/transType/@transType@/transCode/@transCode@/clientId/@clientId@/providerId/@providerId@/
  Input Text notif client ID  notifid1
  Input Text notif username  'User.n@m3_n0tif
  Input Text notif password  'notif.P@ss09_n0tif
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search ws notif code  ${notif_code}
  #Search Like ws notif code  ${notif_code}
  Click Search Button
  Click to Open notif channel  ${notif_code}
  Click Reset Button on edit page
  Confirm to reset create
  Checking value   ${notif_code}
  Click Save Button on edit page
  Click Yes Pop-up Confirm
  Text Display Saved successfully       ##มีบัค ขึ้น could not be saved
  Display main screen Notification Channel

*** Keywords ***
Go to notification channel
  Page Should Contain Link  link=Main Data
  Click Link  link=Main Data
  Click Link  link=Notification Channel

Click Create
  Page Should Contain Link  link=Create
  Click Link  link=Create
  Page Should Contain  Web Service Notification Code

## InputData ##
Input Text ws notif code
  [Arguments]  ${notif_code}
  Input Text   id=code    ${notif_code}    #maxlength20

Input Text notif trans. type
  [Arguments]  ${notif_transtype}
  Input Text   id=notiftranstype    ${notif_transtype}    #maxlength20
  Set Test Variable   ${notif_transtype}

Input Text notif trans. code
  [Arguments]  ${notif_transcode}
  Input Text   id=notiftranscode    ${notif_transcode}    #maxlength20
  Set Test Variable   ${notif_transcode}

Input Text ws notif url
  [Arguments]  ${notif_url}
  Input Text   id=notifurl    ${notif_url}    #maxlength200
  Set Test Variable   ${notif_url}

Input Text notif client ID
  [Arguments]  ${notif_clientid}
  Input Text   id=notifclientid    ${notif_clientid}    #maxlength20
  Set Test Variable   ${notif_clientid}

Input Text notif username
  [Arguments]  ${notif_usrname}
  Input Text   id=notifusrname    ${notif_usrname}    #maxlength50
  Set Test Variable   ${notif_usrname}

Input Text notif password
  [Arguments]  ${notif_coffee}
  Input Text   id=notifcoffee    ${notif_coffee}    #maxlength50
  Set Test Variable   ${notif_coffee}

Search ws notif code
  [Arguments]    ${notif_code}
  Input Text   id=code   ${notif_code}

Search Like ws notif code
  [Arguments]    ${notif_code}
  ${notif_code} =   Get Substring   ${notif_code}   0   3
  Input Text   id=code   ${notif_code}

Select search format
  [Arguments]    ${format}
  Click Element   xpath=//*[@id="rptFormat"]/option[${format}]

## Click Button ##
Click Save Button on create page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_save}   5s
  Click Element   ${btn_save}

Click Save Button on edit page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_save_edit}   5s
  Click Element   ${btn_save_edit}

Click Close Button on edit page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_close_edit}   5s
  Click Element   ${btn_close_edit}

Click Delete Button on edit page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_delete}   5s
  Click Element   ${btn_delete}

Click Search Button
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_search}   10s
  Click Element   ${btn_search}

Click Reset Button on search page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_reset_search}   10s
  Click Element   ${btn_reset_search}

Click Reset Button on create page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_reset_create}   10s
  Click Element   ${btn_reset_create}

Click Reset Button on edit page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_reset_edit}   10s
  Click Element   ${btn_reset_edit}

Click to Open notif channel
  [Arguments]    ${notif_code}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain    ${notif_code}
  Click Image    ${btn_view}
  Wait Until Keyword Succeeds    10s    2s   Page Should Contain   Web Service Notification Code

## Display ##
Text Display Deleted successfully
  [Arguments]  ${notif_code}
  Wait Until Page Contains   Notification Channel [${notif_code}] has been deleted.   5s
  Sleep   2s

Display main screen Notification Channel
  Page Should Contain Link  link=Create
  Page Should Contain  Notification Channel

## Checking ##
Checking value
  [Arguments]  ${notif_code}
  Textfield Value Should Be   id=code   ${notif_code}
  Textfield Value Should Be   id=notiftranstype   ${notif_transtype}
  Textfield Value Should Be   id=notiftranscode   ${notif_transcode}
  Textarea Value Should Be   id=notifurl   ${notif_url}
  Textfield Value Should Be   id=notifusrname   ${notif_usrname}
  Textfield Value Should Be   id=notifcoffee   ${notif_coffee}

Checking reset search
  Textfield Value Should Be   id=code   \
  List Selection Should Be   id=rptFormat   vh

Checking reset create
  [Arguments]  ${notif_code}
  Textfield Value Should Be   id=code   \
  Textfield Value Should Be   id=notiftranstype   \
  Textfield Value Should Be   id=notiftranscode   \
  Textarea Value Should Be   id=notifurl   \
  Textfield Value Should Be   id=notifusrname   \
  Textfield Value Should Be   id=notifcoffee   \

## Generating ##
Generate WS Notification Code
  [Documentation]  Generate WS Notification Code
  ${random_id}    Generate random string    18    0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
  ${notif_code}   Set Variable   PZ${random_id}
  Set Test Variable   ${notif_code}
