*** Settings ***
Documentation  Merchant Classification: This function support only modification.
Resource   ../Resources/BrowserLoginKeyword.robot
Suite Setup    Go To GUI
Test Setup   Go to merchant classification
Test Teardown   Go to Home
Suite Teardown  Close Browser

*** Variables ***
${btn_view}  /maindata/mercate/view?model.id=2
${btn_save_edit}   //*[@id="btnSave"]
${btn_close_edit}   //*[@id="btnCancel"]
${btn_reset_edit}   //*[@id="input-form"]/div[2]/div/input[2]

*** Test Cases ***
TC_01 Update: successfully and restore to original value
  [Tags]    success
  Click to Open detail
  Get merchant classification name
  Get mdr rate
  Input Text merchant classification name  BeMerchant_tesT_เทส
  Input mdr rate  1.2222
  Click Save Button on edit page
  confirm to save
  Click to Open detail
  Checking value
  Input Text merchant classification name  ${merclass_name_orig}
  Input mdr rate  ${mdrrate_orig}
  Click Save Button on edit page
  confirm to save
  Click to Open detail
  Checking value
  Click Close Button on edit page
  Checking value on search result

TC_02 Update: successfully and restore to original value
  [Tags]    success
  Click to Open detail
  Get merchant classification name
  Get mdr rate
  Input Text merchant classification name  BeMerchant_tesT_เทส
  Input mdr rate  99.9999
  Click Save Button on edit page
  confirm to save
  Click to Open detail
  Checking value
  Input Text merchant classification name  ${merclass_name_orig}
  Input mdr rate  ${mdrrate_orig}
  Click Save Button on edit page
  confirm to save
  Click to Open detail
  Checking value
  Click Close Button on edit page
  Checking value on search result

TC_03 Update: Merchant Classification Name already exists
  [Tags]    failure
  Click to Open detail
  Input Text merchant classification name  ipay
  Click Save Button on edit page
  Text Display alert message   Merchant Classification Name already exists.
  Click Close Pop-up alert

TC_04 Update: Merchant Classification Name must be specified
  [Tags]    failure
  Click to Open detail
  Input Text merchant classification name  \
  Click Save Button on edit page
  Text Display alert message   Merchant Classification Name must be specified.
  Click Close Pop-up alert

TC_05 Update: Invalided MDR Rate
  [Tags]    failure
  Click to Open detail
  Input mdr rate  101.0000
  Click Save Button on edit page
  Text Display alert message   Invalided MDR Rate(%)
  Click Close Pop-up alert

TC_06 Update: MDR Rate(%) must be specified.
  [Tags]    failure
  Click to Open detail
  Input mdr rate  \
  Click Save Button on edit page
  Text Display alert message   MDR Rate(%) must be specified.
  Click Close Pop-up alert

TC_07 Update: Padding mdr rate
  [Tags]    success
  Click to Open detail
  Input mdr rate  1
  Click Save Button on edit page
  Click No Pop-up Confirm
  checking mdr rate padding 4 decimal
  Click Close Button on edit page

TC_08 Reset button on edit page and saved without change
  [Tags]    success
  Click to Open detail
  Get merchant classification name
  Get mdr rate
  Input Text merchant classification name  BeMerchant_tesT_เทส
  Input mdr rate  1.2222
  Click Reset Button on edit page
  Confirm to reset edit
  Checking value orig before reset
  Click Save Button on edit page
  Click Yes Pop-up Confirm
  Text Display Saved successfully
  Checking value on search result no change

*** Keywords ***
Go to merchant classification
  Page Should Contain Link  link=Main Data
  Click Link  link=Main Data
  Click Link  link=Merchant Classification

## InputData ##
Input Text merchant classification name
  [Arguments]  ${merclass_name}
  Input Text   id=name    ${merclass_name}    #maxlength50
  Set Test Variable   ${merclass_name}

Input mdr rate
  [Arguments]  ${mdrrate}
  Input Text    id=mdrrate   ${mdrrate}
  Set Test Variable   ${mdrrate}

## Click Button ##
Click Save Button on create page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_save}   5s
  Click Element   ${btn_save}

Click Save Button on edit page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_save_edit}   5s
  Click Element   ${btn_save_edit}

Click Close Button on edit page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_close_edit}   5s
  Click Element   ${btn_close_edit}

Click Delete Button on edit page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_delete}   5s
  Click Element   ${btn_delete}

Click Search Button
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_search}   10s
  Click Element   ${btn_search}

Click Reset Button on search page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_reset_search}   10s
  Click Element   ${btn_reset_search}

Click Reset Button on create page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_reset_create}   10s
  Click Element   ${btn_reset_create}

Click Reset Button on edit page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_reset_edit}   10s
  Click Element   ${btn_reset_edit}

Click to Open detail
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain    Merchant Classification
  Click Link    ${btn_view}
  Wait Until Keyword Succeeds    10s    2s   Page Should Contain   Merchant Classification Name

Confirm to Save
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to save this item ?
  sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
  Click Yes Pop-up Confirm

Confirm to Delete
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to delete this item ?
  sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
  Click Yes Pop-up Confirm

Confirm to Reset create
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to reset this item ?
  sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
  Click Yes Pop-up Confirm

Confirm to Reset edit
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to reset this form ?
  sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
  Click Yes Pop-up Confirm

## Display ##

## Checking ##
Checking value
  Textfield Value Should Be   id=name   ${merclass_name}
  Textfield Value Should Be   id=mdrrate   ${mdrrate}

Checking value orig before reset
  Textfield Value Should Be   id=name   ${merclass_name_orig}
  Textfield Value Should Be   id=mdrrate   ${mdrrate_orig}

Checking value on search result
  Table Should Contain   id=result   ${merclass_name}
  Table Should Contain   id=result   ${mdrrate}
  Table Should Contain   id=result   ${LOGINUSERNAME}

Checking value on search result no change
  Table Should Contain   id=result   ${merclass_name_orig}
  Table Should Contain   id=result   ${mdrrate_orig}
  Table Should Contain   id=result   ${LOGINUSERNAME}

Checking mdr rate padding 4 decimal
  Click Element   id=mdrrate
  Textfield Value Should Be   id=mdrrate   1.0000   5s

## Generating ##

## Getting Value ##
Get merchant classification name
  ${merclass_name_orig}   Get Value   id=name
  Set Test Variable   ${merclass_name_orig}

Get mdr rate
  ${mdrrate_orig}   Get Value   id=mdrrate
  Set Test Variable   ${mdrrate_orig}