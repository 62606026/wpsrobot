*** Settings ***
Documentation  I'm robot and I executing my source code.
Resource   ../Resources/BrowserLoginKeyword.robot
Suite Setup    Go To GUI
Test Setup   Go to Users
Test Teardown   Go to Home
Suite Teardown  Close Browser

*** Variables ***
${length_username}  5
${maxlength_username}   50
${btn_save}  //*[@id="btnSubmit"]
${btn_search}  //*[@id="btnSubmit"]
${btn_view}   //*[@id="content"]/table/tbody/tr[2]/td[2]/span
${btn_reset}  //*[@id="btnReset"]   #copy Xpath
${btn_close}  //*[@id="btnCancel"]  #copy Xpath
${btn_delete}  //*[@id="btnDelete"]     #copy Xpath
${btn_reset_pwd}  //*[@id="btnResetPass"]

*** Test Cases ***
TC_01 Create: successfully.
  [Tags]   success
  Generate Test User    # generate random user for easy test
  Click Create
  Input Username  ${user_name}
  Select User Type   1
  Select Group Name   1
  Input First Name-Last Name    chutima    r.
  Input Email   chutima@sycapt.com
  Click Save Button
  Confirm to Save
  Text Display create successfully   ${user_name}
  Sleep    1 seconds
  Search Username   ${user_name}
  Click Search Button
  Display Search Username   ${user_name}
  Click to Open Username   ${user_name}
  Checking values is valid and Close Page   ${user_name}

TC_02 Create: First-Last name is empty
  [Tags]   success
  Generate Test User
  Click Create
  Input Username  ${user_name}
  Select User Type   1
  Select Group Name   1
  Input Email   chutima@sycapt.com
  Click Save Button
  Confirm to Save
  Text Display create successfully   ${user_name}
  Search Username   ${user_name}
  Click Search Button
  Display Search Username   ${user_name}

TC_03 Create: with Password Never Expires
  [Tags]    success
  Generate Test User
  Click Create
  Input Username  ${user_name}
  Select User Type   1
  Select Group Name   1
  Input Email   chutima@sycapt.com
  Selected Never Expires
  Click Save Button
  Confirm to Save
  Text Display create successfully   ${user_name}
  Search Username   ${user_name}
  Click Search Button
  Display Search Username   ${user_name}
  Click to Open Username   ${user_name}
  Checking values is valid with password never exp   ${user_name}
  Scroll Page To Location   0    200
  Click Close Button
  Confirm to Close

TC_04 Create: username maximum length
  [Tags]   success
  Generate Test User maximum characters
  Click Create
  Input Username  ${user_name_maxlength}
  Select User Type   1
  Select Group Name   1
  Input Email   chutima@sycapt.com
  Click Save Button
  Confirm to Save
  Text Display create successfully   ${user_name_maxlength}
  Search Username   ${user_name_maxlength}
  Click Search Button
  Display Search Username   ${user_name_maxlength}

TC_05 Create: input username with thai letters.
  [Tags]   failure
  Click Create
  Input Username with thai letters
  Click Save Button
  Text Display alert message  Username is in an invalid format.
  Click Close Pop-up alert

TC_06 Create: input username with specaial characters.
  [Tags]   failure
  Click Create
  Input Username with specaial characters
  Click Save Button
  Text Display alert message  Username is in an invalid format.
  Click Close Pop-up alert

TC_07 Create input username less than x characters.
  [Tags]   failure
  Generate Test User less than x characters
  Click Create
  Input Username less than x characters   ${user_name_length}
  Click Save Button
  Text Display alert message  Username is at least 5 characters !!
  Click Close Pop-up alert

TC_08 Create: Username is empty.
  [Tags]   failure
  Click Create
  Click Save Button
  Text Display alert message  Username must be specified.
  Click Close Pop-up alert

TC_09 Create: Username already exists
  [Tags]   failure
  Click Create
  Input Username already exists
  Click Save Button
  Text Display alert message  Username : ROBOTGIRL already exists.
  Click Close Pop-up alert

TC_10 Create: Email is an invalid format
  [Tags]   failure
  Generate Test User
  Click Create
  Input Username  ${user_name}
  Select User Type   1
  Select Group Name   1
  Input Email   chutima999--#&@sycapt.com
  Click Save Button
  Text Display alert message  Email is in an invalid format.
  Click Close Pop-up alert

TC_11 Create: Email is empty
  [Tags]   failure
  Generate Test User
  Click Create
  Input Username  ${user_name}
  Select User Type   1
  Select Group Name   1
  Click Save Button
  Text Display alert message  Email must be specified.
  Click Close Pop-up alert

TC_12 Create: User Type is empty
  [Tags]   failure
  Generate Test User
  Click Create
  Input Username  ${user_name}
  Select Group Name   1
  Input Email   chutima@sycapt.com
  Click Save Button
  Text Display alert message  User Type must be specified.
  Click Close Pop-up alert

TC_13 Reset button on create page
  [Tags]   success
  Generate Test User
  Click Create
  Input Username  ${user_name}
  Select User Type   1
  Select Group Name   1
  Input First Name-Last Name    chutima     r.
  Input Email   chutima@sycapt.com
  Click Reset Button
  Confirm to Reset
  Checking values after reset on create page

TC_14 Delete: Successfully
  [Tags]   success
  Generate Test User
  Click Create
  Input Username  ${user_name}
  Select User Type   1
  Select Group Name   1
  Input First Name-Last Name    chutima    r.
  Input Email   chutima@sycapt.com
  Click Save Button
  Confirm to Save
  Text Display create successfully   ${user_name}
  Search Username   ${user_name}
  Click Search Button
  Display Search Username   ${user_name}
  Click to Open Username   ${user_name}
  Scroll Page To Location   0    200
  Click Delete Button
  Confirm to Delete   ${user_name}
  Text Display delete successfully  ${user_name}
  Search Username   ${user_name}
  Click Search Button
  Text Display Data is not found

TC_15 Create and Reset password
  [Tags]   success
  Generate Test User
  Click Create
  Input Username  ${user_name}
  Select User Type   1
  Select Group Name   1
  Input First Name-Last Name    chutima    r.
  Input Email   chutima@sycapt.com
  Click Save Button
  Confirm to Save
  Text Display create successfully   ${user_name}
  Search Username   ${user_name}
  Click Search Button
  Display Search Username   ${user_name}
  Click to Open Username    ${user_name}
  Scroll Page To Location    0    200
  Click Reset Password Button
  Confirm to Reset Password   ${user_name}
  Text Display reset password successfully   ${user_name}

TC_16 Close button on create page
  [Tags]    success
  Click Create
  Click Close Button
  confirm to close

TC_17 Create and Update successfully
  [Tags]    success
  Generate Test User    # generate random user for easy test
  Click Create
  Input Username  ${user_name}
  Select User Type   1
  Select Group Name   1
  Input First Name-Last Name    chutima    r.
  Input Email   chutima@sycapt.com
  Click Save Button
  Confirm to Save
  Text Display create successfully   ${user_name}
  Sleep    1 seconds
  Search Username   ${user_name}
  Click Search Button
  Display Search Username   ${user_name}
  Click to Open Username   ${user_name}
  Checking values is valid   ${user_name}
  Select Group Name   2
  Input First Name-Last Name    ชุติมา    รวีวรรณากร
  Input Email   chutima8888@sycapt.com
  Selected Never Expires
  Selected disabled user
  Checking values is valid after edit  ${user_name}
  Scroll Page To Location   0    200
  Click Close Button
  Confirm to close

TC_18 Reset button on edit page.
  [Tags]    success
  Generate Test User    # generate random user for easy test
  Click Create
  Input Username  ${user_name}
  Select User Type   1
  Select Group Name   1
  Input First Name-Last Name    chutima    r.
  Input Email   chutima@sycapt.com
  Click Save Button
  Confirm to Save
  Text Display create successfully   ${user_name}
  Sleep    1 seconds
  Search Username   ${user_name}
  Click Search Button
  Display Search Username   ${user_name}
  Click to Open Username   ${user_name}
  Checking values is valid   ${user_name}
  Scroll Page To Location   0    200
  Click Reset Button
  Confirm to Reset
  Checking values is valid   ${user_name}
  Scroll Page To Location   0    200
  Click Close Button
  Confirm to close

*** Keywords ***
Go to Users
  Page Should Contain Link  link=Administration
  Click Link  link=Administration
  Click Link  link=Users

Click Create
  Page Should Contain Link  link=Create
  Click Link  link=Create
  Page Should Contain  Users

Input Username
  [Arguments]  ${user_name}
  Input Text  name=usrname  ${user_name}

Input Username with thai letters
  Input Text  name=usrname  ทดสอบ123

Input Username with specaial characters
  Input Text  name=usrname  test123@#$

Input Username less than x characters
  [Arguments]  ${user_name_length}
  Input Text  name=usrname  ${user_name_length}

Input Username maximum characters
  [Arguments]  ${user_name_maxlength}
  Input Text  name=usrname  ${user_name_maxlength}

Input Username already exists
  Input Text  name=usrname  ${LOGINUSERNAME}

Select User Type
  [Arguments]  ${usrtype}
  Click Element  xpath=//select[@name="grpType"]/option[@value=${usrtype}]  ##user type 1=Bank
  Set Test Variable   ${usrtype}

Select Group Name
  [Arguments]  ${grpname}
  Click Element  xpath=//select[@name="grpid"]/option[@value=${grpname}]  ##grp name=Sycapt
  Set Test Variable   ${grpname}

Input First Name-Last Name
  [Arguments]   ${fname}     ${lname}
  Input Text  name=fname    ${fname}
  Input Text  name=lname    ${lname}
  Set Test Variable   ${fname}
  Set Test Variable   ${lname}

Input Email
  [Arguments]  ${email}
  Input Text  name=email  ${email}
  Set Test Variable   ${email}

Selected Never Expires
  Select Checkbox   name=neverexp

Selected disabled user
  Select Checkbox   name=disabled

Click Save Button
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_save}   10s
  Click Element   ${btn_save}

Confirm to Save
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to save this item ?
  ## because pop up did not show at first time when the web is open
  ## use javascript to read xpath will be work
  ##Execute JavaScript    document.evaluate("${popup_btn_yes}", document, null, XPathResult.ANY_TYPE, null).iterateNext().click()
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm

Click Search Button
  Wait Until Element Is Visible   ${btn_search}   10s
  Click Element   ${btn_search}

Click Reset Button
  Wait Until Element Is Visible   ${btn_reset}   5s
  Click Element   ${btn_reset}

Confirm to Reset
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain   Do you want to reset this form ?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm

Checking values after reset on create page
  Textfield Value Should Be  name=usrname  ${EMPTY}
  List Selection Should Be  grpType  0
  List Selection Should Be  grpid  0
  Textfield Value Should Be  name=fname  ${EMPTY}
  Textfield Value Should Be  name=lname  ${EMPTY}
  Textfield Value Should Be  name=email  ${EMPTY}

Click Close Button
  Wait Until Element Is Visible   ${btn_close}   5s
  Click Element   ${btn_close}

Confirm to Close
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain   Do you want to close this page ?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm
  Page Should Contain Link  link=Create

Click Delete Button
  Wait Until Element Is Visible   ${btn_delete}   5s
  Click Element   ${btn_delete}

Confirm to Delete
  [Arguments]  ${user_name}
  ${user_name}   Convert To Uppercase   ${user_name}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain   Do you want to delete ${user_name} ?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm
  Page Should Contain Link  link=Create

Click Reset Password Button
  Wait Until Element Is Visible   ${btn_reset_pwd}   5s
  Click Element   ${btn_reset_pwd}

Confirm to Reset Password
  [Arguments]  ${user_name}
  ${user_name}   Convert To Uppercase   ${user_name}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain   Do you want to reset password ${user_name} ?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm

Text Display create successfully
  [Arguments]    ${user_name}
  ${user_name}   Convert To Uppercase   ${user_name}
  Wait Until Page Contains  Create username ${user_name} successfully   5s

Text Display delete successfully
  [Arguments]    ${user_name}
  ${user_name}   Convert To Uppercase   ${user_name}
  Wait Until Page Contains  Username ${user_name} has been deleted.   5s

Text Display Data is not found
  Wait Until Page Contains  Data is not found.   5s

Text Display reset password successfully
  [Arguments]    ${user_name}
  ${user_name}   Convert To Uppercase   ${user_name}
  Wait Until Page Contains  Send email username ${user_name} and password is successfully.   5s
  #Sleep    3 seconds

Search Username
  [Arguments]    ${user_name}
  Input Text  name=searchusrname  ${user_name}

Display Search Username
  [Arguments]    ${user_name}
  ${user_name}   Convert To Uppercase   ${user_name}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  ${user_name}

Click to Open Username
  [Arguments]    ${user_name}
  ${user_name}   Convert To Uppercase   ${user_name}
  Click Element  ${btn_view}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  ${user_name}

Checking values is valid and Close Page
  [Arguments]    ${user_name}
  ${user_name}   Convert To Uppercase   ${user_name}
  Element Text Should Be  //*[@id="user-form"]/table/tbody/tr[1]/td[3]/span   ${user_name}
  List Selection Should Be  grpType  ${usrtype}
  List Selection Should Be  grpid  ${grpname}
  Textfield Value Should Be  name=fname  ${fname}
  Textfield Value Should Be  name=lname  ${lname}
  Textfield Value Should Be  name=email  ${email}
  Scroll Page To Location    0    200
  Click Close Button
  Confirm to Close
  Page Should Contain Link  link=Create

Checking values is valid
  [Arguments]    ${user_name}
  ${user_name}   Convert To Uppercase   ${user_name}
  Element Text Should Be  //*[@id="user-form"]/table/tbody/tr[1]/td[3]/span   ${user_name}
  List Selection Should Be  id=grpType  ${usrtype}
  List Selection Should Be  id=grpid  ${grpname}
  Textfield Value Should Be  name=fname  ${fname}
  Textfield Value Should Be  name=lname  ${lname}
  Textfield Value Should Be  name=email  ${email}
  Checkbox Should Not Be Selected   id=disabled
  Checkbox Should Not Be Selected   id=neverexp

Checking values is valid with password never exp
  [Arguments]    ${user_name}
  ${user_name}   Convert To Uppercase   ${user_name}
  Element Text Should Be  //*[@id="user-form"]/table/tbody/tr[1]/td[3]/span   ${user_name}
  List Selection Should Be  id=grpType  ${usrtype}
  List Selection Should Be  id=grpid  ${grpname}
  Textfield Value Should Be  name=fname  \
  Textfield Value Should Be  name=lname  \
  Textfield Value Should Be  name=email  ${email}
  Checkbox Should Not Be Selected   id=disabled
  Checkbox Should Be Selected   id=neverexp

Checking values is valid after edit
  [Arguments]    ${user_name}
  ${user_name}   Convert To Uppercase   ${user_name}
  Element Text Should Be  //*[@id="user-form"]/table/tbody/tr[1]/td[3]/span   ${user_name}
  List Selection Should Be  id=grpType  ${usrtype}
  List Selection Should Be  id=grpid  ${grpname}
  Textfield Value Should Be  name=fname  ${fname}
  Textfield Value Should Be  name=lname  ${lname}
  Textfield Value Should Be  name=email  ${email}
  Checkbox Should Be Selected   id=disabled
  Checkbox Should Be Selected   id=neverexp

Generate Test User
    [Documentation]  Generate Test User
    ${random_id}    Generate random string    5    0123456789
    ${user_name}   Set Variable   user${random_id}
    Set Test Variable   ${user_name}

Generate Test User less than x characters
    [Documentation]  Generate Test User
    ${random_id_length}    Generate random string   4    0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ
    ${user_name_length}    Set Variable   ${random_id_length}
    Set Test Variable   ${user_name_length}

Generate Test User maximum characters
    [Documentation]  Generate Test User
    ${random_id_maxlength}    Generate random string   50    0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ
    ${user_name_maxlength}    Set Variable   ${random_id_maxlength}
    Set Test Variable   ${user_name_maxlength}