*** Settings ***
Documentation  I'm robot and I executing my source code.
Resource   ../Resources/BrowserLoginKeyword.robot
Suite Setup    Go To GUI
Test Setup   Go to Groups
Test Teardown   Go to Home
Suite Teardown  Close Browser

*** Variables ***
${length_groupname}  5
${btn_save}  //*[@id="btnSubmit"]
${btn_reset}  //*[@id="group-form"]/div/input[2]
${btn_search}  //*[@id="btnSubmit"]
${btn_delete}  //*[@id="btnDelete"]
${btn_close}  //*[@id="divBtn"]/ul/table/tbody/tr/td[2]/li/input[3]
${login_grp}    SYCAPT

*** Test Cases ***
TC_01 Create: Successfully.
  [Tags]    success
  Generate Group Name
  Click Create
  Input Group Name   ${group_name}
  Select Group Type
  Select All Permission
  Scroll Page To Location   0   6000
  Click Save Button
  Confirm to Save
  Text Display save group successfully
  Search Group Name   ${group_name}
  Click Search Button
  Checking group name is valid and Close Page   ${group_name}

TC_02 Create: input groupname less than x characters.
  [Tags]    failure
  Generate Group Name less than x characters
  Click Create
  Input Group name less than x characters   ${group_name_length}
  Select Group Type
  Select All Permission
  Scroll Page To Location   0   6000
  Click Save Button
  Text Display alert message  Group name must be at least ${length_groupname} characters.
  Click Close Pop-up alert

TC_03 Create: groupname is empty.
  [Tags]    failure
  Click Create
  Select Group Type
  Select All Permission
  Scroll Page To Location   0   6000
  Click Save Button
  Text Display alert message  Group Name must be specified.
  Click Close Pop-up alert

TC_04 Create: groupname already exists.
  [Tags]    failure
  Click Create
  Input group name already exists
  Select Group Type
  Select All Permission
  Scroll Page To Location   0   6000
  Click Save Button
  Text Display alert message  Group Name already exists.
  Click Close Pop-up alert

TC_05 Create: grouptype is empty.
  [Tags]    failure
  Generate Group Name
  Click Create
  Input Group Name   ${group_name}
  Select All Permission
  Scroll Page To Location   0   6000
  Click Save Button
  Text Display alert message  Group Type must be specified.
  Click Close Pop-up alert

TC_06 Create: permission is empty.
  [Tags]    failure
  Generate Group Name
  Click Create
  Input Group Name   ${group_name}
  Select Group Type
  Scroll Page To Location   0   6000
  Click Save Button
  Text Display alert message  Permission must be selected.
  Click Close Pop-up alert

TC_07 Delete: Successfully
  [Tags]    success
  Generate Group Name
  Click Create
  Input Group Name   ${group_name}
  Select Group Type
  Select All Permission
  Scroll Page To Location   0   6000
  Click Save Button
  Confirm to Save
  Text Display save group successfully
  Search Group Name   ${group_name}
  Click Search Button
  Scroll Page To Location   0   6000
  Click Delete Button
  Confirm to Delete   ${group_name}
  Text Display delete successfully   ${group_name}
  Search Group Name   ${group_name}
  Click Search Button
  Text Display Data is not found

TC_08 Update: existing group and delete successfully.
  [Tags]    success
  Generate Group Name
  Click Create
  Input Group Name   ${group_name}
  Select Group Type
  Select All Permission
  Scroll Page To Location   0   6000
  Click Save Button
  Confirm to Save
  Text Display save group successfully
  Search Group Name   ${group_name}
  Click Search Button
  Checking group name is valid   ${group_name}
  Generate New Group Name
  Input New Group Name
  Change Group Type
  Unselect Some Permission
  Scroll Page To Location   0   6000
  Click Save Button
  Confirm to Save
  Text Display save group successfully
  Search New Group Name   ${new_group_name}
  Click Search Button
  Checking group name is valid for update   ${new_group_name}
  Scroll Page To Location   0   6000
  Click Delete Button
  Confirm to Delete New Group   ${new_group_name}
  Text Display delete successfully for new group   ${new_group_name}
  Search New Group Name   ${new_group_name}
  Click Search Button
  Text Display Data is not found

*** Keywords ***
Go to Groups
  Page Should Contain Link  link=Administration
  Click Link  link=Administration
  Click Link  link=Groups

Click Create
  Page Should Contain Link  link=Create
  Click Link  link=Create
  Page Should Contain  Groups

Input Group Name
  [Arguments]  ${group_name}
  Input Text  name=model.grpName   ${group_name}

Input Group name less than x characters
  [Arguments]  ${group_name_length}
  Input Text  name=model.grpName   ${group_name_length}

Input Group name already exists
  Input Text    name=model.grpName  ${login_grp}

Input New Group Name
  Clear Element Text    //*[@id="grpName"]
  Input Text    name=model.grpName  ${new_group_name}

Select Group Type
  Click Element  //*[@id="grpTypeId"]/option[2]  ##grp type=bank

Change Group Type
  Click Element  //*[@id="grpTypeId"]/option[3]  ##grp type=bank

Select All Permission
  Select Checkbox  name=chkCmd0  #select view
  Select Checkbox  name=chkCmd1  #select create
  Select Checkbox  name=chkCmd2  #select update
  Select Checkbox  name=chkCmd3  #select delete
  Select Checkbox  name=chkCmd4  #select export

Unselect Some Permission
  Unselect Checkbox  name=chkCmd1  #select create
  Unselect Checkbox  name=chkCmd2  #select update
  Unselect Checkbox  name=chkCmd3  #select delete
  Unselect Checkbox  name=chkCmd4  #select export

Click Save Button
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_save}   5s
  Click Element   //*[@id="btnSubmit"]

Confirm to Save
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to save this item ?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm

Click Reset Button
  Wait Until Element Is Visible   ${btn_reset}   5s
  Click Element   ${btn_reset}

Click Close Button
  Wait Until Element Is Visible   ${btn_close}   5s
  Click Element   ${btn_close}

Click Search Button
  Wait Until Element Is Visible   ${btn_search}   10s
  Click Element   ${btn_search}

Confirm to Close
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain   Do you want to close this page ?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm

Click Delete Button
  Wait Until Element Is Visible   ${btn_delete}   5s
  Click Element   ${btn_delete}

Confirm to Delete
  [Arguments]  ${group_name}
  ${group_name}   Convert To Uppercase   ${group_name}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain   Do you want to delete ${group_name} ?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm
  Page Should Contain Link  link=Create

Confirm to Delete New Group
  [Arguments]  ${new_group_name}
  ${new_group_name}   Convert To Uppercase   ${new_group_name}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain   Do you want to delete ${new_group_name} ?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm
  Page Should Contain Link  link=Create

Search Group Name
  [Arguments]    ${group_name}
  Input Text  name=model.grpName  ${group_name}

Search New Group Name
  [Arguments]    ${new_group_name}
  Input Text  name=model.grpName  ${new_group_name}

Text Display save group successfully
  Wait Until Page Contains   Saved successfully.   5s

Text Display delete successfully
  [Arguments]    ${group_name}
  ${group_name}   Convert To Uppercase   ${group_name}
  Wait Until Page Contains  Groups '${group_name}' has been deleted.   5s

Text Display delete successfully for new group
  [Arguments]    ${new_group_name}
  ${new_group_name}   Convert To Uppercase   ${new_group_name}
  Wait Until Page Contains  Groups '${new_group_name}' has been deleted.   5s

Text Display Data is not found
  Wait Until Page Contains  Data is not found.   5s

Checking group name is valid and Close Page
  [Arguments]    ${group_name}
  ${group_name}   Convert To Uppercase   ${group_name}
  Textfield Value Should Be  name=model.grpName  ${group_name}
  List Selection Should Be  model.grpTypeId  1
  Checkbox Should Be Selected  name=chkCmd0
  Checkbox Should Be Selected  name=chkCmd1
  Checkbox Should Be Selected  name=chkCmd2
  Checkbox Should Be Selected  name=chkCmd3
  Checkbox Should Be Selected  name=chkCmd4
  Scroll Page To Location   0   6000
  Click Close Button
  Confirm to Close
  Page Should Contain Link  link=Create

Checking group name is valid
  [Arguments]    ${group_name}
  ${group_name}   Convert To Uppercase   ${group_name}
  Textfield Value Should Be  name=model.grpName  ${group_name}
  List Selection Should Be  model.grpTypeId  1
  Checkbox Should Be Selected  name=chkCmd0
  Checkbox Should Be Selected  name=chkCmd1
  Checkbox Should Be Selected  name=chkCmd2
  Checkbox Should Be Selected  name=chkCmd3
  Checkbox Should Be Selected  name=chkCmd4

Checking group name is valid for update
  [Arguments]    ${new_group_name}
  ${new_group_name}   Convert To Uppercase   ${new_group_name}
  Textfield Value Should Be  name=model.grpName  ${new_group_name}
  List Selection Should Be  model.grpTypeId  2
  Checkbox Should Not Be Selected   name=chkCmd1
  Checkbox Should Not Be Selected   name=chkCmd2
  Checkbox Should Not Be Selected   name=chkCmd3
  Checkbox Should Not Be Selected   name=chkCmd4
  Checkbox Should Be Selected   name=chkCmd0

Generate Group Name
    [Documentation]  Generate Group Name
    ${random_id}    Generate random string    5    0123456789
    ${group_name}   Set Variable   GRP_${random_id}
    Set Test Variable   ${group_name}

Generate Group Name less than x characters
    [Documentation]  Generate Group Name
    ${random_id_length}    Generate random string   4    0123456789
    ${group_name_length}    Set Variable   ${random_id_length}
    Set Test Variable   ${group_name_length}

Generate New Group Name
    [Documentation]  Generate New Group Name
    ${new_text}     Generate random string  3    XYZ0123456789
    ${new_group_name}   Set Variable    NEW_GRP_${new_text}
    Set Test Variable   ${new_group_name}
