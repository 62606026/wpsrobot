*** Settings ***
Documentation  I'm robot and I executing my source code.
Resource   ../Resources/BrowserLoginKeyword.robot
Suite Setup    Go To GUI
Test Setup   Go to Merchants
Test Teardown   Go to Home
#Suite Teardown  Close Browser

*** Variables ***
${btn_save}  //*[@id="btnSubmit"]
${btn_delete}  //*[@id="btnDelete"]
${btn_search}  //*[@id="search-mer"]/table[2]/tbody/tr[15]/td[2]/input[1]
${btn_view}  //*[@id="result"]/tbody/tr[1]/td[2]/a/img
${btn_close}   //*[@id="mer-form"]/div/div/div/div[5]/table/tbody/tr/td[2]/input[3]
${btn_close_create}   //*[@id="mer-form"]/div/div/div/div[4]/table/tbody/tr/td[2]/input[3]
${chainid}   000002021029352     #iPay 000002021029352, BeMer 000002021095704, General 000002021084839

*** Test Cases ***
TC_01 Create: chain successfully.
  [Tags]   success
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Input contact name    คุณมานะ ทำงาน
  Input contact phone   025557777
  Input contact email    email_test@test.com
  Input contact fax
  Scroll Page To Location    0    400
  select allow web service transaction
  regenerate signkey
  Scroll Page To Location  0  0
  Tab Alipay
  Tab Wechat
  select wechat info
  Scroll Page To Location    0    400
  Click Save Button
  Confirm to Save
  Text Display Saved successfully
  Search Merchant ID  ${mer_id}
  Scroll Page To Location    0    400
  Click Search Button
  Click to Open Merchant   ${mer_id}
  Checking merchant chain is valid and Close Page   ${mer_id}

TC_02 Create: BeMerchant chain successfully.
  [Tags]   success
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   2
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Input contact name    คุณมานะ ทำงาน
  Input contact phone   025557777
  Input contact email    email_test@test.com
  Input contact fax
  Scroll Page To Location    0    400
  select allow web service transaction
  regenerate signkey
  Select qr push notify
  Scroll Page To Location  0  0
  Tab Alipay
  Tab Wechat
  select wechat info
  Scroll Page To Location    0    400
  Click Save Button
  Confirm to Save
  Text Display Saved successfully
  Search Merchant ID  ${mer_id}
  Scroll Page To Location    0    400
  Click Search Button
  Click to Open Merchant   ${mer_id}
  Checking merchant chain is valid and Close Page   ${mer_id}

TC_03 Create: Child Individual successfully.
  [Tags]   success
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type   2
  Select chain id  ${chainid}
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Input address prov zip   9/888 m.16 soi3 /-23=493lksdfl   bangkok   10330
  Input storeID   ${random_id}
  Scroll Page To Location    0    200
  Select MCC   9399
  Input office tel
  Select orgtype   Individual
  Input Mer tax
  Input Mer tax name    ${mer_regis_name}
  Input contact name    คุณชาบู ลายพลาง
  Input contact phone   023337777
  Input contact email    email_test111@test.com
  Scroll Page To Location    0    1000
  Select mer currency
  Select credit mer currency
  select allow web service transaction
  regenerate signkey
  Input mer name on QR  ${mer_qr_name}
  Input telegram ID
  Scroll Page To Location    0    0
  Tab Alipay
    Select alipay partner
  Tab Wechat
    Select Wechat partner
    Input Mer short name   ${mer_short_name}
  Tab Alipay
  Click Save Button
  Confirm to Save
  Text Display Saved successfully
  Search Merchant ID  ${mer_id}
  Scroll Page To Location    0    400
  Click Search Button
  Click to Open Merchant   ${mer_id}
  Checking merchant child is valid and Close Page   ${mer_id}

TC_04 Create: standalone Individual successfully.
  [Tags]   present1
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type   3
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Input address prov zip   9/888 m.16 soi3 <>/-23=493lksdfl   bangkok   10330
  Input storeID   11
  Scroll Page To Location    0    200
  Select MCC   9399
  Input office tel
  Select orgtype   Individual
  Input Mer tax
  Input Mer tax name    ${mer_regis_name}
  Input contact name    คุณชาบู ลายพลาง
  Input contact phone   023337777
  Input contact email    email_test111@test.com
  Scroll Page To Location    0    1000
  Select response message mapping
  Select MDR Type  D
  Select mer currency
  Select credit mer currency
  select allow web service transaction
  regenerate signkey
  Input mer name on QR  ${mer_qr_name}
  Input telegram ID
  Scroll Page To Location    0    0
  Tab Alipay
    Select alipay partner
  Tab Wechat
    Select Wechat partner
    select wechat info
    Input Mer short name   ${mer_short_name}
  Tab Alipay
  Click Save Button
  Confirm to Save
  Text Display Saved successfully
  Search Merchant ID  ${mer_id}
  Scroll Page To Location    0    400
  Click Search Button
  Click to Open Merchant   ${mer_id}
  Checking merchant standalone is valid and Close Page   ${mer_id}

TC_05 Create: standalone Enterprise successfully.
  [Tags]   success
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type   3
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Input address prov zip   9/888 m.16 soi3 <>/-23=493lksdfl   bangkok   10330
  Input storeID   11
  Scroll Page To Location    0    200
  Select MCC   9399
  Input office tel
  Select orgtype   Enterprise
  Input Mer tax
  Input Mer tax name    ${mer_regis_name}
  Input contact name    คุณทำงาน ขยันเวอร์
  Input contact phone   025557777
  Input contact email    email_test@test.com
  Scroll Page To Location    0    1000
  Select response message mapping
  Select mer currency
  Select credit mer currency
  select allow web service transaction
  regenerate signkey
  Input mer name on QR  ${mer_qr_name}
  Input telegram ID
  Scroll Page To Location    0    0
  Tab Alipay
    Select alipay partner
  Tab Wechat
    Select Wechat partner
    select wechat info
    Input Mer short name   ${mer_short_name}
  Tab Alipay
  Click Save Button
  Confirm to Save
  Text Display Saved successfully
  Search Merchant ID  ${mer_id}
  Scroll Page To Location    0    400
  Click Search Button
  Click to Open Merchant   ${mer_id}
  Checking merchant standalone is valid and Close Page   ${mer_id}

TC_06 Create: BeMerchant standalone Individual successfully.
  [Tags]   success
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   2
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type   3
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Input address prov zip   9/888 m.16 soi3 <>/-23=493lksdfl   bangkok   10330
  Input storeID   11
  Scroll Page To Location    0    200
  Select MCC   9399
  Input office tel
  Select orgtype   Individual
  Input Mer tax
  Input Mer tax name    ${mer_regis_name}
  Input contact name    คุณชาบู ลายพลาง
  Input contact phone   023337777
  Input contact email    email_test111@test.com
  Scroll Page To Location    0    1000
  Select response message mapping
  Select mer currency
  Select credit mer currency
  select allow web service transaction
  regenerate signkey
  Input mer name on QR  ${mer_qr_name}
  Select qr push notify
  Input telegram ID
  Scroll Page To Location    0    0
  Tab Alipay
    Select alipay partner
  Tab Wechat
    Select Wechat partner
    select wechat info
    Input Mer short name   ${mer_short_name}
  Tab Alipay
  Click Save Button
  Confirm to Save
  Text Display Saved successfully
  Search Merchant ID  ${mer_id}
  Scroll Page To Location    0    400
  Click Search Button
  Click to Open Merchant   ${mer_id}
  Checking BeMerchant standalone is valid and Close Page   ${mer_id}

TC_07 Create: chain successfully without optional fields.
  [Tags]   success
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input contact name    คุณมานะ ทำงาน
  Input contact phone   025557777
  Input contact email    email_test@test.com
  Scroll Page To Location    0    400
  Select MDR Type   M
  Scroll Page To Location  0  0
  Tab Alipay
  Tab Wechat
  Scroll Page To Location    0    400
  Click Save Button
  Confirm to Save
  Text Display Saved successfully
  Search Merchant ID  ${mer_id}
  Scroll Page To Location    0    400
  Click Search Button
  Click to Open Merchant   ${mer_id}
  Checking merchant chain without optional field is valid and Close Page   ${mer_id}

TC_08 Create: chain successfully with specify rate.
  [Tags]   success
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input contact name    คุณมานะ ทำงาน
  Input contact phone   025557777
  Input contact email    email_test@test.com
  Scroll Page To Location    0    400
  Select MDR Type   M
  Select response message mapping
  Scroll Page To Location  0  0
  Tab Alipay
  Select alipay specify mdr rate   2.5
  Tab Wechat
  Select wechat specify mdr rate   2
  Select wechat specify markup rate   0.75
  Scroll Page To Location    0    400
  Click Save Button
  Confirm to Save
  Text Display Saved successfully
  Search Merchant ID  ${mer_id}
  Scroll Page To Location    0    400
  Click Search Button
  Click to Open Merchant   ${mer_id}
  Checking merchant chain with specify rate is valid and Close Page   ${mer_id}

TC_09 Create: Merchant Number must be specified
  [Tags]   failure
  Click Create
  Scroll Page To Location   0   2000
  Click Save Button
  Text Display alert message   Merchant Number must be specified.
  Click Close Pop-up alert

TC_10 Create: Merchant Number already exists
  [Tags]   failure
  Click Create
  Input Merchant Number already exists
  Scroll Page To Location   0   2000
  Click Save Button
  Text Display alert message    Merchant Number already exists.
  Click Close Pop-up alert

TC_11 Padding Merchant Number
  [Tags]   success
  Click Create
  Input Merchant Number less than 15 digits
  Checking merchant padding
  Scroll Page To Location    0    400
  Click Close Button on create page
  Confirm to Close

TC_12 Create: Merch. DBA Name(EN) must be specified
  [Tags]   failure
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Input Merchant ID 1-15 digits   ${mer_id}
  Scroll Page To Location   0   2000
  Click Save Button
  Text Display alert message    Merch. DBA Name(EN) must be specified.
  Click Close Pop-up alert

TC_13 Create: Merch. DBA Name(EN) with thai char
  [Tags]   success
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Input Merchant ID 1-15 digits   ${mer_id}
  Input Merchant Name EN with thai char
  Scroll Page To Location   0   2000
  Click Save Button
  Text Display alert message    Merch. DBA Name(EN) can contain only English letters, numbers, and special characters.
  Click Close Pop-up alert

TC_14 Create: Contact Information must be specify at least one row
  [Tags]   failure
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Scroll Page To Location    0    400
  Click Save Button
  Text Display alert message    Contact Information must be specify at least one row.
  Click Close Pop-up alert

TC_15 Create: Phone must be specified
  [Tags]   failure
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Input contact name    คุณมานี่่ มีหม้อ
  Scroll Page To Location    0    400
  Click Save Button
  Text Display alert message   Phone must be specified.
  Click Close Pop-up alert

TC_16 Create: Email must be specified
  [Tags]   failure
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Input contact name    คุณมานี่่ มีหม้อ
  Input contact phone  087885845
  Scroll Page To Location    0    400
  Click Save Button
  Text Display alert message   Email must be specified.
  Click Close Pop-up alert

TC_17 Create: Email invalid format
  [Tags]   present2
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Input contact name    คุณมานี่่ มีหม้อ
  Input contact phone  087885845
  Input contact email   sdfk0#@fkdlfk.e45@dfs.v
  Scroll Page To Location    0    400
  Click Save Button
  Text Display alert message   Email is in an invalid format.
  Click Close Pop-up alert

TC_18 Create: No select any wallet host
  [Tags]   failure
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Input contact name    คุณมานี่่ มีหม้อ
  Input contact phone  087885845
  Input contact email   test_test@test.com
  Scroll Page To Location    0    400
  No select any wallet host
  Click Save Button
  Text Display alert message   Wallet Host must be selected.
  Click Close Pop-up alert

TC_19 Create: Sign key must be specified
  [Tags]   failure
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Input contact name    คุณมานี่่ มีหม้อ
  Input contact phone  087885845
  Input contact email   test_test@test.com
  Scroll Page To Location    0    400
  Select allow web service transaction
  Click Save Button
  Text Display alert message   Sign key must be specified.
  Click Close Pop-up alert

TC_20 Create: MDR Rate(%) must be specified. Wallet1
  [Tags]  failure
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input contact name    คุณมานะ ทำงาน
  Input contact phone   025557777
  Input contact email    email_test@test.com
  Scroll Page To Location    0    400
  Select MDR Type   M
  Select response message mapping
  Scroll Page To Location  0  0
  Tab Alipay
  Select alipay specify mdr rate   \
  Click Save Button
  Text Display alert message   MDR Rate(%) must be specified.
  Click Close Pop-up alert

TC_21 Create: MDR Rate(%) must be specified. Wallet2
  [Tags]   failure
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input contact name    คุณมานะ ทำงาน
  Input contact phone   025557777
  Input contact email    email_test@test.com
  Scroll Page To Location    0    400
  Select MDR Type   M
  Select response message mapping
  Scroll Page To Location  0  0
  Tab Wechat
  Select wechat specify mdr rate  \
  Scroll Page To Location    0    400
  Click Save Button
  Text Display alert message   MDR Rate(%) must be specified.
  Click Close Pop-up alert

TC_22 Create: Markup Rate(%) must be specified. Wallet2
  [Tags]   failure
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type    1
  Input Merchant Name EN  ${mer_name_en}
  Input contact name    คุณมานะ ทำงาน
  Input contact phone   025557777
  Input contact email    email_test@test.com
  Scroll Page To Location    0    400
  Select MDR Type   M
  Select response message mapping
  Scroll Page To Location  0  0
  Tab Wechat
  Select wechat specify markup rate   \
  Scroll Page To Location    0    400
  Click Save Button
  Text Display alert message   Markup Rate(%) must be specified.
  Click Close Pop-up alert

TC_23 Delete: standalone successfully.
  [Tags]   success
  Generate Test Merchant ID
  Generate Test Merchant Name
  Click Create
  Select Classification   1
  Input Merchant ID 1-15 digits   ${mer_id}
  Select Merchant Type   3
  Input Merchant Name EN  ${mer_name_en}
  Input Merchant Name TH  ${mer_name_th}
  Input address prov zip   9/888 m.16 soi3 <>/-23=493lksdfl   bangkok   10330
  Input storeID   11
  Scroll Page To Location    0    200
  Select MCC   9399
  Input office tel
  Select orgtype   Individual
  Input Mer tax
  Input Mer tax name    ${mer_regis_name}
  Input contact name    คุณมานะ ทำงาน
  Input contact phone   025557777
  Input contact email    email_test@test.com
  Scroll Page To Location    0    1000
  Select response message mapping
  Select mer currency
  Select credit mer currency
  select allow web service transaction
  regenerate signkey
  Input mer name on QR  ${mer_qr_name}
  Input telegram ID
  Scroll Page To Location    0    0
  Tab Alipay
    Select alipay partner
  Tab Wechat
    Select Wechat partner
    Input Mer short name   ${mer_short_name}
    select wechat info
  Tab Alipay
  Click Save Button
  Confirm to Save
  Text Display Saved successfully
  Search Merchant ID  ${mer_id}
  Scroll Page To Location    0    400
  Click Search Button
  Click to Open Merchant   ${mer_id}
  Scroll Page To Location    0    2000
  Click Delete Button
  Confirm to Delete   ${mer_id}
  Text Display Deleted successfully   ${mer_id}
  Search Merchant ID  ${mer_id}
  Scroll Page To Location    0    400
  Click Search Button
  Scroll Page To Location    0    400
  Text Display Data is not found

*** Keywords ***
Go to Merchants
  Page Should Contain Link  link=Main Data
  Click Link  link=Main Data
  Click Link  link=Merchants

Click Create
  Page Should Contain Link  link=Create
  Click Link  link=Create
  Page Should Contain  Merchant Infomation

Tab Merchant Info
  Click Element  //*[@id="tabs"]/li[1]/a
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Merchant Infomation

Tab Alipay
  Click Element  //*[@id="tabs"]/li[2]/a
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  MDR Info.

Tab Wechat
  Click Element  //*[@id="tabs"]/li[3]/a
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Business Category

Select Alipay partner
  Click Element   xpath=//*[@id="managemodel.wallethosts[1].merpartnerList[0].partnerid"]/option[4]

Select alipay specify mdr rate
  [Arguments]  ${alipaymdr}
  Select Radio Button   managemodel.wallethosts[1].mdr.mdrratemode      0
  Input Text    id=wallethost-1-mdrrate   ${alipaymdr}

Select wechat specify mdr rate
  [Arguments]  ${wechatmdr}
  Select Radio Button   managemodel.wallethosts[2].mdr.mdrratemode      0
  Input Text    id=wallethost-2-mdrrate   ${wechatmdr}

Select wechat specify markup rate
  [Arguments]  ${wechatmarkup}
  Select Radio Button   managemodel.wallethosts[2].mu.mumode      0
  Input Text    id=wallethost-2-murate   ${wechatmarkup}

Select Wechat partner
  #Click Element   xpath=//*[@id="managemodel.wallethosts[2].merpartnerList[0].partnerid"]/option[3]
  Select From List By Value   id=managemodel.wallethosts[2].merpartnerList[0].partnerid   2

Select Wechat info
  Click Element   xpath=//*[@id="bizcatid"]/option[6]
  Input Text   id=prodname   Robot girl by pukki can create new merchant with alipay and wechat info now!!

Select Classification
  [Arguments]  ${class}
  Select Radio Button   managemodel.mer.merclassificationid      managemodel_mer_merclassificationid${class}   #name=   id=
  Set Test Variable   ${class}

Select chain id
  [Arguments]  ${chain}
  Click Element   xpath=//*[@id="select2-chosen-1"]
  Input Text   id=s2id_autogen1_search   ${chain}
  Press Keys   id=s2id_autogen1_search   RETURN

Select Merchant Type
  [Arguments]  ${mertype}
  Select Radio Button   managemodel.mer.mertype     ${mertype}    #merchant type chain

Select MDR Type
  [Arguments]  ${mdrtype}
  Select Radio Button   managemodel.mer.mdrperiod     ${mdrtype}    #merchant type chain

No select any wallet host
   Unselect Checkbox   id=listwallethostchild-1

Input Merchant ID 1-15 digits
  [Arguments]  ${mer_id}
  Input Text  name=managemodel.mer.merid   ${mer_id}

Input Merchant Number already exists
  Input Text  name=managemodel.mer.merid  202100151720475

Input Merchant Number less than 15 digits
  Input Text  name=managemodel.mer.merid  202100151

Input Dolfin Merchant ID 1-32 digits
  [Arguments]  ${mer_id}
  Input Text  name=model.dfnmid   ${mer_id}

Input Merchant Name EN
  [Arguments]  ${mer_name_en}
  Input Text  name=managemodel.mer.mernameEn  ${mer_name_en}

Input Merchant Name EN with thai char
  Input Text  name=managemodel.mer.mernameEn  สวัสดี

Input Merchant Name TH
  [Arguments]  ${mer_name_th}
  Input Text  name=managemodel.mer.mernameTh  ${mer_name_th}

Input Mer short name
  [Arguments]  ${mer_short_name}
  Input Text  name=managemodel.mer.mershortnameEn  ${mer_short_name}

Input Mer tax
  Input Text   id=taxid   9999999999994

Input Mer tax name
  [Arguments]  ${mer_regis_name}
  Input Text  id=taxmername  ${mer_regis_name}

Input address prov zip
   [Arguments]  ${addr}   ${prov}   ${zip}
   Input Text   id=addr1   ${addr}
   Input Text   id=provnameEn   ${prov}
   Input Text   id=zipcode   ${zip}

Input StoreID
   [Arguments]  ${storeid}
   Input Text   id=storeid   ${storeid}

Select MCC
   [Arguments]  ${mcc}
   Click Element   xpath=//*[@id="select2-chosen-2"]
   Input Text   id=s2id_autogen2_search   ${mcc}
   Press Keys   xpath=//*[@id="s2id_autogen2_search"]   RETURN

Select orgtype
   [Arguments]  ${org}
   Click Element   xpath=//*[@id="select2-chosen-3"]
   Input Text   id=s2id_autogen3_search   ${org}
   Press Keys   id=s2id_autogen3_search   RETURN
   Set Test Variable   ${org}

Select response message mapping
   Click Element   xpath=//*[@id="select2-chosen-7"]
   Input Text   id=s2id_autogen7_search   king
   Press Keys   id=s2id_autogen7_search   RETURN

Input office tel
  Input Text    name=managemodel.mer.tel    021515500

Input contact name
  [Arguments]  ${contactname}
  Input Text    name=managemodel.contact.contactname    ${contactname}
  Set Test Variable   ${contactname}

Input contact phone
  [Arguments]   ${contacttel}
  Input Text    name=managemodel.contact.contacttel    ${contacttel}
  Set Test Variable   ${contacttel}

Input contact email
    [Arguments]  ${contactemail}
    Input Text    name=managemodel.contact.contactemail    ${contactemail}
    Set Test Variable   ${contactemail}

Input contact fax
    Input Text   id=contactfax_0    025548888

Select mer currency
   Click Element   xpath=//*[@id="select2-chosen-4"]
   Input Text   id=s2id_autogen4_search   THB
   Press Keys   id=s2id_autogen4_search   RETURN

Select credit mer currency
   Click Element   xpath=//*[@id="select2-chosen-5"]
   Input Text   id=s2id_autogen5_search   THB
   Press Keys   id=s2id_autogen5_search   RETURN

Input mer name on QR
  [Arguments]  ${mer_qr_name}
  Input Text  name=managemodel.mer.qrmername  ${mer_qr_name}

Select qr push notify
   Click Element   xpath=//*[@id="select2-chosen-6"]
   Input Text   id=s2id_autogen6_search   BEMERCHANT
   Press Keys   id=s2id_autogen6_search   RETURN

Select allow web service transaction
  Select Checkbox   id=allowwstrans

Regenerate signkey
    Click Element   //*[@id="allowwstransDiv"]/td[3]/input[2]
    Wait Until Keyword Succeeds   10s  2s   Page Should Contain   Do you want to Re-Generate Web Service Sign Key ?
    Click Yes Pop-up Confirm

Input telegram ID
  Input Text   id=tlgusername_0   G-255-043-837-33

Click Save Button
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_save}   5s
  Click Element   ${btn_save}

Click Delete Button
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   xpath=/html/body/div[1]/div[4]/form/div/div/div/div[5]/table/tbody/tr/td[1]/input   5s
  Click Element    xpath=/html/body/div[1]/div[4]/form/div/div/div/div[5]/table/tbody/tr/td[1]/input

Click Search Button
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_search}   10s
  Click Element   ${btn_search}

Confirm to Save
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to save this item ?
  sleep    2s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
  Click Yes Pop-up Confirm

Confirm to Delete
  [Arguments]   ${mer_id}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to delete ${mer_id} ?
  sleep    2s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
  Click Yes Pop-up Confirm

Click Close Button
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_close}   5s
  Click Element   ${btn_close}

Confirm to Close
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain   Do you want to close this page ?
  Wait Until Keyword Succeeds   10s  2s   Click Yes Pop-up Confirm
  Page Should Contain Link  link=Create

Click to Open Merchant
  [Arguments]    ${mer_id}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain    ${mer_id}
  Scroll Page To Location    0    1000
  Click Image    //*[@id="result"]/tbody/tr[1]/td[2]/a/img
  Wait Until Keyword Succeeds    10s    2s   Page Should Contain  Merchant Infomation

Click Title Form
  Click Element  //*[@id="title-form"]
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Merchant No.

Text Display Deleted successfully
  [Arguments]   ${mer_id}
  Wait Until Page Contains   Merchants ${mer_id} has been deleted.   5s
  Sleep   2s

Checking merchant padding
  Click Element   id=mernameen
  Textfield Value Should Be   name=managemodel.mer.merid   000000202100151   5s

Text Display Store Name can contain only English letters
  Wait Until Page Contains   Store Name can contain only English letters, numbers, and special characters.   5s
  Click Close Pop-up alert

Search Merchant ID
  [Arguments]    ${mer_id}
  Input Text  name=model.merid  ${mer_id}

Display Search Merchant ID
  [Arguments]    ${mer_id}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  ${mer_id}
  sleep    1s

Checking merchant chain is valid and Close Page
  [Arguments]    ${mer_id}
  Radio Button Should Be Set To    managemodel.mer.merclassificationid   ${class}
  Textfield Value Should Be  name=managemodel.mer.merid   ${mer_id}
  Radio Button Should Be Set To    managemodel.mer.mertype   1
  Textfield Value Should Be  name=managemodel.mer.mernameEn   ${mer_name_en}
  Textfield Value Should Be  name=managemodel.mer.mernameTh   ${mer_name_th}
  Radio Button Should Be Set To    managemodel.mer.merstatus   A
  Textfield Value Should Be  id=contactname_0    ${contactname}
  Textfield Value Should Be  id=contacttel_0    ${contacttel}
  Textfield Value Should Be  id=contactemail_0    ${contactemail}
  Textfield Value Should Be  id=contactfax_0    025548888
  Scroll Page To Location   0   2000
  Checkbox Should Be Selected   id=listwallethostchild-1
  Radio Button Should Be Set To    managemodel.mer.mdrperiod   D
  Checkbox Should Be Selected   id=allowwstrans
  Textfield Should Contain   id=signkey   -
  Tab Alipay
    Radio Button Should Be Set To    managemodel.wallethosts[1].mdr.mdrratemode   2
    Radio Button Should Be Set To    managemodel.wallethosts[1].walletperm.allow   1
  Tab Wechat
    List Selection Should Be   id=bizcatid   490
    Textarea Value Should Be  id=prodname   Robot girl by pukki can create new merchant with alipay and wechat info now!!
  Click Close Button
  Confirm to Close
  Page Should Contain Link  link=Create

Checking merchant child is valid and Close Page
  [Arguments]    ${mer_id}
  Textfield Value Should Be  name=managemodel.mer.merid   ${mer_id}
  Radio Button Should Be Set To    managemodel.mer.mertype   2   #1chain 2child 3standalone
  Textfield Value Should Be  name=managemodel.mer.mernameEn   ${mer_name_en}
  Textfield Value Should Be  name=managemodel.mer.mernameTh   ${mer_name_th}
  Textarea Value Should Be  id=addr1   9/888 m.16 soi3 /-23=493lksdfl
  Textfield Value Should Be  id=provnameEn   bangkok
  Textfield Value Should Be  id=zipcode   10330
  Radio Button Should Be Set To    managemodel.mer.merstatus   A
  Scroll Page To Location   0    600
  List Selection Should Be   id=mcc   9399
  Textfield Value Should Be  id=tel   021515500
  Current Frame Should Contain   ${org}
  Textfield Value Should Be  id=taxid   9999999999994
  Textfield Value Should Be  id=taxmername   ${mer_regis_name}
  Textfield Value Should Be  id=contactname_0    ${contactname}
  Textfield Value Should Be  id=contacttel_0    ${contacttel}
  Textfield Value Should Be  id=contactemail_0    ${contactemail}
  Scroll Page To Location   0   2000
  Checkbox Should Be Selected   id=listwallethostchild-1
  Radio Button Should Be Set To    managemodel.mer.paymentmode   3   #3=all
  List Selection Should Be   id=mercurr   THB
  List Selection Should Be   id=clrcurr   THB
  Radio Button Should Be Set To    managemodel.mer.allowrefund   0
  Radio Button Should Be Set To    managemodel.mer.guiallowrefund   0
  Radio Button Should Be Set To    managemodel.mer.wsallowrefund   0
  Checkbox Should Be Selected   id=allowwstrans
  Textfield Should Contain   id=signkey   -
  Radio Button Should Be Set To    managemodel.mer.samereqidmode   3
  Textfield Value Should Be  managemodel.mer.qrmername    ${mer_qr_name}
  Checkbox Should Be Selected   id=isqrtransnotice
  Radio Button Should Be Set To    managemodel.mer.isrcvqrtransdet    1
  Radio Button Should Be Set To    managemodel.mer.isrcvsumqrtransrpt    1
  Textfield Should Contain   id=tlgusername_0   G-255-043-837-33
  Tab Alipay
    List Selection Should Be   id=managemodel.wallethosts[1].merpartnerList[0].partnerid   1
    Radio Button Should Be Set To    managemodel.wallethosts[1].mdr.mdrratemode   1
  Tab Wechat
    List Selection Should Be   id=managemodel.wallethosts[2].merpartnerList[0].partnerid   2
    List Selection Should Be   id=bizcatid   490
    Textfield Value Should Be  id=mershortnameEn   ${mer_short_name}
    Textarea Value Should Be  id=prodname   Robot girl by pukki can create new merchant with alipay and wechat info now!!
  Scroll Page To Location   0    600
  Click Close Button
  Confirm to Close
  Page Should Contain Link  link=Create

Checking merchant standalone is valid and Close Page
  [Arguments]    ${mer_id}
  Radio Button Should Be Set To    managemodel.mer.merclassificationid   ${class}
  Textfield Value Should Be  name=managemodel.mer.merid   ${mer_id}
  Radio Button Should Be Set To    managemodel.mer.mertype   3   #1chain 2child 3standalone
  Textfield Value Should Be  name=managemodel.mer.mernameEn   ${mer_name_en}
  Textfield Value Should Be  name=managemodel.mer.mernameTh   ${mer_name_th}
  Textarea Value Should Be  id=addr1   9/888 m.16 soi3 <>/-23=493lksdfl
  Textfield Value Should Be  id=provnameEn   bangkok
  Textfield Value Should Be  id=zipcode   10330
  Radio Button Should Be Set To    managemodel.mer.merstatus   A
  Textfield Value Should Be  id=storeid   11
  Scroll Page To Location   0    600
  List Selection Should Be   id=mcc   9399
  Textfield Value Should Be  id=tel   021515500
  Current Frame Should Contain   ${org}
  Textfield Value Should Be  id=taxid   9999999999994
  Textfield Value Should Be  id=taxmername   ${mer_regis_name}
  Textfield Value Should Be  id=contactname_0    ${contactname}
  Textfield Value Should Be  id=contacttel_0    ${contacttel}
  Textfield Value Should Be  id=contactemail_0    ${contactemail}
  Scroll Page To Location   0   2000
  Checkbox Should Be Selected   id=listwallethostchild-1
  Radio Button Should Be Set To    managemodel.mer.paymentmode   3   #3=all
  Radio Button Should Be Set To    managemodel.mer.mdrperiod   D
  List Selection Should Be   id=mercurr   THB
  List Selection Should Be   id=clrcurr   THB
  Radio Button Should Be Set To    managemodel.mer.allowrefund   0
  Radio Button Should Be Set To    managemodel.mer.guiallowrefund   0
  Radio Button Should Be Set To    managemodel.mer.wsallowrefund   0
  Checkbox Should Be Selected   id=allowwstrans
  Textfield Should Contain   id=signkey   -
  Radio Button Should Be Set To    managemodel.mer.samereqidmode   3
  Textfield Value Should Be  managemodel.mer.qrmername    ${mer_qr_name}
  List Selection Should Be   id=respmsgmapprofid   13
  Checkbox Should Be Selected   id=isqrtransnotice
  Radio Button Should Be Set To    managemodel.mer.isrcvqrtransdet    1
  Radio Button Should Be Set To    managemodel.mer.isrcvsumqrtransrpt    1
  Textfield Should Contain   id=tlgusername_0   G-255-043-837-33
  Tab Alipay
    List Selection Should Be   id=managemodel.wallethosts[1].merpartnerList[0].partnerid   1
    Radio Button Should Be Set To    managemodel.wallethosts[1].mdr.mdrratemode   2
    Radio Button Should Be Set To    managemodel.wallethosts[1].walletperm.allow   1
  Tab Wechat
    List Selection Should Be   id=managemodel.wallethosts[2].merpartnerList[0].partnerid   2
    List Selection Should Be   id=bizcatid   490
    Textfield Value Should Be  id=mershortnameEn   ${mer_short_name}
    Textarea Value Should Be  id=prodname   Robot girl by pukki can create new merchant with alipay and wechat info now!!
  Scroll Page To Location   0    600
  Click Close Button
  Confirm to Close
  Page Should Contain Link  link=Create

Checking BeMerchant standalone is valid and Close Page
  [Arguments]    ${mer_id}
  Radio Button Should Be Set To    managemodel.mer.merclassificationid   ${class}
  Textfield Value Should Be  name=managemodel.mer.merid   ${mer_id}
  Radio Button Should Be Set To    managemodel.mer.mertype   3   #1chain 2child 3standalone
  Textfield Value Should Be  name=managemodel.mer.mernameEn   ${mer_name_en}
  Textfield Value Should Be  name=managemodel.mer.mernameTh   ${mer_name_th}
  Textarea Value Should Be  id=addr1   9/888 m.16 soi3 <>/-23=493lksdfl
  Textfield Value Should Be  id=provnameEn   bangkok
  Textfield Value Should Be  id=zipcode   10330
  Radio Button Should Be Set To    managemodel.mer.merstatus   A
  Textfield Value Should Be  id=storeid   11
  Scroll Page To Location   0    600
  List Selection Should Be   id=mcc   9399
  Textfield Value Should Be  id=tel   021515500
  Current Frame Should Contain   ${org}
  Textfield Value Should Be  id=taxid   9999999999994
  Textfield Value Should Be  id=taxmername   ${mer_regis_name}
  Textfield Value Should Be  id=contactname_0    ${contactname}
  Textfield Value Should Be  id=contacttel_0    ${contacttel}
  Textfield Value Should Be  id=contactemail_0    ${contactemail}
  Scroll Page To Location   0   2000
  Checkbox Should Be Selected   id=listwallethostchild-1
  Radio Button Should Be Set To    managemodel.mer.paymentmode   3   #3=all
  Radio Button Should Be Set To    managemodel.mer.mdrperiod   D
  List Selection Should Be   id=mercurr   THB
  List Selection Should Be   id=clrcurr   THB
  Radio Button Should Be Set To    managemodel.mer.allowrefund   0
  Radio Button Should Be Set To    managemodel.mer.guiallowrefund   0
  Radio Button Should Be Set To    managemodel.mer.wsallowrefund   0
  Checkbox Should Be Selected   id=allowwstrans
  Textfield Should Contain   id=signkey   -
  Radio Button Should Be Set To    managemodel.mer.samereqidmode   3
  Textfield Value Should Be  managemodel.mer.qrmername    ${mer_qr_name}
  List Selection Should Be   id=bankpushchannel   1
  List Selection Should Be   id=respmsgmapprofid   13
  Checkbox Should Be Selected   id=isqrtransnotice
  Radio Button Should Be Set To    managemodel.mer.isrcvqrtransdet    1
  Radio Button Should Be Set To    managemodel.mer.isrcvsumqrtransrpt    1
  Textfield Should Contain   id=tlgusername_0   G-255-043-837-33
  Tab Alipay
    List Selection Should Be   id=managemodel.wallethosts[1].merpartnerList[0].partnerid   1
    Radio Button Should Be Set To    managemodel.wallethosts[1].mdr.mdrratemode   2
    Radio Button Should Be Set To    managemodel.wallethosts[1].walletperm.allow   1
  Tab Wechat
    List Selection Should Be   id=managemodel.wallethosts[2].merpartnerList[0].partnerid   2
    List Selection Should Be   id=bizcatid   490
    Textfield Value Should Be  id=mershortnameEn   ${mer_short_name}
    Textarea Value Should Be  id=prodname   Robot girl by pukki can create new merchant with alipay and wechat info now!!
  Scroll Page To Location   0    600
  Click Close Button
  Confirm to Close
  Page Should Contain Link  link=Create

Checking merchant chain without optional field is valid and Close Page
  [Arguments]    ${mer_id}
  Radio Button Should Be Set To    managemodel.mer.merclassificationid   ${class}
  Textfield Value Should Be  name=managemodel.mer.merid   ${mer_id}
  Radio Button Should Be Set To    managemodel.mer.mertype   1
  Textfield Value Should Be  name=managemodel.mer.mernameEn   ${mer_name_en}
  Textfield Value Should Be  name=managemodel.mer.mernameTh   \     #single backslash \ use for empty string
  Radio Button Should Be Set To    managemodel.mer.merstatus   A
  Textfield Value Should Be  id=contactname_0    ${contactname}
  Textfield Value Should Be  id=contacttel_0    ${contacttel}
  Textfield Value Should Be  id=contactemail_0    ${contactemail}
  Textfield Value Should Be  id=contactfax_0   \
  Scroll Page To Location   0   2000
  Checkbox Should Be Selected   id=listwallethostchild-1
  Radio Button Should Be Set To    managemodel.mer.mdrperiod   M
  Checkbox Should Not Be Selected   id=allowwstrans
  Tab Alipay
    Radio Button Should Be Set To    managemodel.wallethosts[1].mdr.mdrratemode   2
    Radio Button Should Be Set To    managemodel.wallethosts[1].walletperm.allow   1
  Tab Wechat
    Page Should Contain   --- Please Select ---
    Textarea Value Should Be  id=prodname   \
  Click Close Button
  Confirm to Close
  Page Should Contain Link  link=Create

Checking merchant chain with specify rate is valid and Close Page
  [Arguments]    ${mer_id}
  Radio Button Should Be Set To    managemodel.mer.merclassificationid   ${class}
  Textfield Value Should Be  name=managemodel.mer.merid   ${mer_id}
  Radio Button Should Be Set To    managemodel.mer.mertype   1
  Textfield Value Should Be  name=managemodel.mer.mernameEn   ${mer_name_en}
  Textfield Value Should Be  name=managemodel.mer.mernameTh   \     #single backslash \ use for empty string
  Radio Button Should Be Set To    managemodel.mer.merstatus   A
  Textfield Value Should Be  id=contactname_0    ${contactname}
  Textfield Value Should Be  id=contacttel_0    ${contacttel}
  Textfield Value Should Be  id=contactemail_0    ${contactemail}
  Textfield Value Should Be  id=contactfax_0   \
  Scroll Page To Location   0   2000
  Checkbox Should Be Selected   id=listwallethostchild-1
  Radio Button Should Be Set To    managemodel.mer.mdrperiod   M
  Checkbox Should Not Be Selected   id=allowwstrans
  List Selection Should Be   id=respmsgmapprofid   13
  Tab Alipay
    Radio Button Should Be Set To    managemodel.wallethosts[1].mdr.mdrratemode   0
    Textfield Value Should Be  id=wallethost-1-mdrrate   2.5000
    Radio Button Should Be Set To    managemodel.wallethosts[1].walletperm.allow   1
  Tab Wechat
    Page Should Contain   --- Please Select ---
    Textarea Value Should Be  id=prodname   \
    Radio Button Should Be Set To    managemodel.wallethosts[2].mdr.mdrratemode   0
    Textfield Value Should Be  id=wallethost-2-mdrrate  2.0000
    Radio Button Should Be Set To    managemodel.wallethosts[2].mu.mumode   0
    Textfield Value Should Be  id=wallethost-2-murate  0.7500
  Click Close Button
  Confirm to Close
  Page Should Contain Link  link=Create

Generate Test Merchant ID
    [Documentation]  Generate Test Merchant ID
    ${random_id}    Generate random string    5    0123456789
    ${mer_id}   Set Variable   0000020210${random_id}
    Set Test Variable   ${mer_id}
    Set Test Variable   ${random_id}

Generate Test Merchant Name
    [Documentation]  Generate Test Merchant Name
    #${random_id}    Generate random string    3    1234567890
    ${mer_name_en}   Set Variable   Test Merchant'_${random_id}
    ${mer_short_name}   Set Variable   short?><'!@#$%_${random_id}
    ${mer_qr_name}   Set Variable   QR_&^?><'!@#$%_${random_id}
    ${mer_regis_name}   Set Variable   Regis-${random_id}_'efghijklmnopqrstuvwxyZ0123456789!@#$%$%^&*_
    ${mer_name_th}   Set Variable   ร้านค้าทดสอบ_${random_id}
    Set Test Variable   ${mer_name_en}
    Set Test Variable   ${mer_short_name}
    Set Test Variable   ${mer_qr_name}
    Set Test Variable   ${mer_regis_name}
    Set Test Variable   ${mer_name_th}

Generate Test Merchant Name TH
    [Documentation]  Generate Test Merchant Name TH
    #${random_id}    Generate random string    3    1234567890
    ${mer_name_th}   Set Variable   ร้านค้าทดสอบ_${random_id}
    Set Test Variable   ${mer_name_th}

Generate Test App ID
    [Documentation]  Generate Test App ID
    ${random_id}    Generate random string    15    123456789
    ${app_id}   Set Variable   ${random_id}
    Set Test Variable   ${app_id}

Generate Test AES Key
    [Documentation]  Generate Test AES KEY
    ${random_id}    Generate random string    24    123456789abcdefghijklmnopqrstuvwxyz
    ${aes_key}   Set Variable   ${random_id}
    Set Test Variable   ${aes_key}

Generate Test JV Public Key
    [Documentation]  Generate Test JV Public Key
    ${random_id}    Generate random string    64    123456789abcdefghijklmnopqrstuvwxyz
    ${jv_key}   Set Variable   ${random_id}
    Set Test Variable   ${jv_key}

Generate Test Client ID
    [Documentation]  Generate Test Client ID
    ${random_id}    Generate random string    32    123456789abcdefghijklmnopqrstuvwxyz
    ${client_id}   Set Variable   ${random_id}
    Set Test Variable   ${client_id}

Generate Test Client Secret Key
    [Documentation]  Generate Test Client Secret Key
    ${random_id}    Generate random string    16    123456789abcdefghijklmnopqrstuvwxyz
    ${client_key}   Set Variable   ${random_id}
    Set Test Variable   ${client_key}
