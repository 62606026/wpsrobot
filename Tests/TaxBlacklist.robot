*** Settings ***
Documentation  I'm robot and I executing my source code.
Resource   ../Resources/BrowserLoginKeyword.robot
Suite Setup    Go To GUI
Test Setup   Go to Tax ID Blacklist
Test Teardown   Go to Home
Suite Teardown  Close Browser

*** Variables ***
${btn_save}  //*[@id="save"]
${btn_delete}  //*[@id="result"]/tbody/tr/td[2]/a/img
${btn_search}  //*[@id="search-form"]/table/tbody/tr[3]/td/input[1]
${btn_view}  //*[@id="result"]/tbody/tr[1]/td[7]/a/img
${btn_save_create}   //*[@id="save"]
${btn_close_create}   //*[@id="create-form"]/table/tbody/tr[2]/td[2]/input[3]
${btn_reset_create}   //*[@id="create-form"]/table/tbody/tr[2]/td[2]/input[2]
${btn_reset_search}   //*[@id="search-form"]/table/tbody/tr[3]/td/input[2]

*** Test Cases ***
TC_01 Create: successfully
  [Tags]    success
  Click Create
  Input Tax ID  2222222222227
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search Tax ID  ${taxid}
  Click Search Button
  Click Delete Images
  Confirm to Delete
  Text Display Deleted successfully   ${taxid}

TC_02 Create: Tax ID must be 13 digits.
  [Tags]    failure
  Click Create
  Input Tax ID  22222222222
  Click Save Button on create page
  Text Display alert message   Tax ID must be 13 digits.
  Click Close Pop-up alert

TC_03 Create: Tax ID is in an invalid format.
  [Tags]    failure
  Click Create
  Input Tax ID  2222222222222
  Click Save Button on create page
  Text Display alert message   Tax ID is in an invalid format.
  Click Close Pop-up alert

TC_04 Create: Tax ID can contain only numbers.
  [Tags]    failure
  Click Create
  Input Tax ID  abcdfgjhkilop
  Click Save Button on create page
  Text Display alert message   Tax ID can contain only numbers.
  Click Close Pop-up alert

TC_05 Create: Tax ID must be specified.
  [Tags]    failure
  Click Create
  Input Tax ID  \
  Click Save Button on create page
  Text Display alert message   Tax ID must be specified.
  Click Close Pop-up alert

TC_06 Create: Tax ID already exists.
  [Tags]    failure
  Click Create
  Input Tax ID  2222222222227
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search Tax ID  ${taxid}
  Click Search Button
  Sleep   1s
  Click Create
  Input Tax ID  2222222222227
  Click Save Button on create page
  Text Display alert message   Tax ID already exists.
  Click Close Pop-up alert
  Click Close Button on create page
  Confirm to Close
  Search Tax ID  ${taxid}
  Click Search Button
  Click Delete Images
  Confirm to Delete
  Text Display Deleted successfully   ${taxid}

TC_07 Reset button on create page
  [Tags]    success
  Click Create
  Input Tax ID  2222222222227
  Click Reset Button on create page
  Confirm to Reset create
  Checking reset create

TC_08 Close button on create page
  [Tags]    success
  Click Create
  Input Tax ID  2222222222227
  Click Close Button on create page
  Confirm to Close
  Display main screen Tax ID Blacklist

TC_09 Reset button on search page
  [Tags]    success
  Input Tax ID  2222222222227
  Select search format  2
  Click Reset Button on search page
  Checking reset search

TC_10 Delete and Search: successfully
  [Tags]    success
  Click Create
  Input Tax ID  2222222222227
  Click Save Button on create page
  Confirm to Save
  Text Display Saved successfully
  Search Tax ID  ${taxid}
  Click Search Button
  Click Delete Images
  Confirm to Delete
  Text Display Deleted successfully   ${taxid}
  Search Tax ID  ${taxid}
  Click Search Button
  Text Display Data is not found

*** Keywords ***
Go to Tax ID Blacklist
  Page Should Contain Link  link=Main Data
  Click Link  link=Main Data
  Click Link  link=Tax ID Blacklist

Click Create
  Page Should Contain Link  link=Create
  Click Link  link=Create
  Page Should Contain  Tax ID

## InputData ##
Input Tax ID
  [Arguments]  ${taxid}
  Input Text   id=taxid    ${taxid}    #maxlength13
  Set Test Variable   ${taxid}

Search Tax ID
  [Arguments]    ${taxid}
  Input Text   id=taxid   ${taxid}

Select search format
  [Arguments]    ${format}
  Click Element   xpath=//*[@id="rptFormat"]/option[${format}]

## Click Button ##
Click Save Button on create page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_save}   5s
  Click Element   ${btn_save}

Click Save Button on edit page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_save_edit}   5s
  Click Element   ${btn_save_edit}

Click Close Button on edit page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_close_edit}   5s
  Click Element   ${btn_close_edit}

Click Delete Button on edit page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_delete}   5s
  Click Element   ${btn_delete}

Click Search Button
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_search}   10s
  Click Element   ${btn_search}

Click Reset Button on search page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_reset_search}   10s
  Click Element   ${btn_reset_search}

Click Reset Button on create page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_reset_create}   10s
  Click Element   ${btn_reset_create}

Click Reset Button on edit page
  ## Wait Until Element Is Visible   [Element]   [Time out]
  Wait Until Element Is Visible   ${btn_reset_edit}   10s
  Click Element   ${btn_reset_edit}

Confirm to Close
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to close this item ?
  sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
  Click Yes Pop-up Confirm

## Display ##
Text Display Deleted successfully
  [Arguments]  ${taxid}
  Wait Until Page Contains   Tax ID[${taxid}] has been deleted.   5s
  Sleep   2s

Display main screen Tax ID Blacklist
  Page Should Contain Link  link=Create
  Page Should Contain  Tax ID Blacklist
  Page Should Contain  Tax ID
  Page Should Contain  Search

## Checking ##
Checking value
  [Arguments]  ${taxid}
  Table Should Contain   id=result   ${taxid}

Checking reset search
  Textfield Value Should Be   id=taxid   \
  List Selection Should Be   id=rptFormat   vh

Checking reset create
  Textfield Value Should Be   id=taxid   \
