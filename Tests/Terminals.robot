*** Settings ***
Documentation  I'm robot and I executing my source code.
Resource   ../Resources/BrowserLoginKeyword.robot
Suite Setup    Go To GUI
Test Setup   Go to Terminal
Test Teardown   Go to Home
Suite Teardown  Close Browser

*** Variables ***
${btn_search}  //*[@id="btnSubmit"]
${btn_reset_search}   //*[@id="search-form"]/table[2]/tbody/tr[9]/td/input[2]
${btn_view}  //*[@id="result"]/tbody/tr[1]/td[7]/a/img
${btn_save}  //*[@id="btnSubmit"]
${btn_close_create}   //*[@id="view-form"]/table[2]/tbody/tr[14]/td[2]/input[3]
${btn_reset_create}   //*[@id="view-form"]/table[2]/tbody/tr[14]/td[2]/input[2]
${btn_delete}  //*[@id="view-form"]/table[2]/tbody/tr[16]/td[1]/input[1]
${btn_save_edit}   /html/body/div[1]/div[5]/div[2]/div/form/table[2]/tbody/tr[16]/td[2]/input[1]
${btn_close_edit}   //*[@id="view-form"]/table[2]/tbody/tr[16]/td[2]/input[3]
${btn_reset_edit}   //*[@id="view-form"]/table[2]/tbody/tr[16]/td[2]/input[2]
${merid}   999999900110011

*** Test Cases ***
TC_01 Create EDC: Successfully
  [Tags]    success
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input term id  ${termid}
  Input main term id  ${termid}
  Input date of install
  Select allow refund  -1
  Input mer name on qr  ${merqrname}
  Click Save Button on create page
  Click Yes Pop-up Confirm
  Text Display Saved successfully
  Search termid edc
  Click Search Button
  Click to open terminal
  Checking value edc
  Click Close Button on edit page
  Click Yes Pop-up Confirm

TC_02 Create E-commerce: Successfully
  [Tags]    success
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  4
  Input term id  ${termid}
  Input date of install
  Click Save Button on create page
  Click Yes Pop-up Confirm
  Text Display Saved successfully
  Search termid e-comm
  Click Search Button
  Click to open terminal
  Checking value e-comm
  Click Close Button on edit page
  Click Yes Pop-up Confirm

TC_03 Create Static QR: Successfully (original qr = No)
  [Tags]    success
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  3
  Input term id  ${termid}
  Select No original static qr
  Input cashier id  pos1
  Input row product  Cheese burger
  Input row amount  89.00
  Input row mer name on qr  ${merqrname}
  Input row date of install
  Click Save Button on create page
  Click Yes Pop-up Confirm
  Text Display If merchant is General, Please specify telegram
  Click to open telegram
  Input telegram ID  G-255-043-837-33
  Input email  chutima@test.com
  Click Yes Pop-up Confirm
  Pop-up display update telegram successful
  Click Save button after create
  Confirm to Save Terminal
  Text Display Saved successfully
  Search termid static qr
  Click Search Button
  Click to open terminal
  Checking value static qr
  Click Close Button on edit page
  Confirm to Close

TC_04 Create EDC: Merchant Number must be specified.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Click Save Button on create page
  Text Display alert message  Merchant Number must be specified.
  Click Close Pop-up alert

TC_05 Create EDC: Merchant Number is not found.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   34756348576
  Click Save Button on create page
  Text Display alert message  Merchant Number is not found.
  Click Close Pop-up alert

TC_06 Create EDC: Terminal No. must be 8 digits.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Input term id  555
  Click Save Button on create page
  Text Display alert message  Terminal No. must be 8 digits.
  Click Close Pop-up alert

TC_07 Create EDC: Terminal No. can contain only English letters and numbers.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Input term id  5555เทส
  Click Save Button on create page
  Text Display alert message  Terminal No. can contain only English letters and numbers.
  Click Close Pop-up alert

TC_08 Create EDC: Terminal No. with Eng. letters and Delete terminal
  [Tags]    success
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Input term id  5555aaaa
  Input main term id  ${termid}
  Input date of install
  Select allow refund  -1
  Input mer name on qr  ${merqrname}
  Click Save Button on create page
  Click Yes Pop-up Confirm
  Text Display Saved successfully
  Search termid edc
  Click Search Button
  Click to open terminal
  Click Delete Button
  Confirm to Delete   ${termid}
  Text Display Deleted successfully   ${termid}
  Search termid edc
  Click Search Button
  Text Display Data is not found

TC_09 Create EDC: Terminal No. already exists.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Input term id  20001251
  Input main term id  ${termid}
  Click Save Button on create page
  Text Display alert message  Terminal No. already exist.
  Click Close Pop-up alert

TC_10 Create EDC: Terminal No. must be specified.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Click Save Button on create page
  Text Display alert message  Terminal No. must be specified.
  Click Close Pop-up alert

TC_11 Create EDC: Main Term. ID must be specified.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Input term id  ${termid}
  Click Save Button on create page
  Text Display alert message  Main Term. ID must be specified.
  Click Close Pop-up alert

TC_12 Create EDC: Main Term. ID must be 8 digits.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Input term id  ${termid}
  Input main term id  6666
  Click Save Button on create page
  Text Display alert message  Main Term. ID must be 8 digits.
  Click Close Pop-up alert

TC_13 Create EDC: Main Term. ID can contain only English letters and numbers.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Input term id  ${termid}
  Input main term id   55590เทส
  Click Save Button on create page
  Text Display alert message  Main Term. ID can contain only English letters and numbers.
  Click Close Pop-up alert

TC_14 Create EDC: Main Term. ID with Eng. letters and Delete terminal
  [Tags]    success
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Input term id  ${termid}
  Input main term id  aaaaaaaa
  Input date of install
  Select allow refund  -1
  Input mer name on qr  ${merqrname}
  Click Save Button on create page
  Click Yes Pop-up Confirm
  Text Display Saved successfully
  Search termid edc
  Click Search Button
  Click to open terminal
  Click Delete Button
  Confirm to Delete   ${termid}
  Text Display Deleted successfully   ${termid}
  Search termid edc
  Click Search Button
  Text Display Data is not found

TC_15 Create EDC: Date of Installation must be specified.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Input term id  ${termid}
  Input main term id  ${termid}
  Click Save Button on create page
  Text Display alert message  Date of installation must be specified.
  Click Close Pop-up alert

TC_16 Create EDC: Day out of valid range.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Input term id  ${termid}
  Input main term id  ${termid}
  Input date of install manual  32122016
  Click Save Button on create page
  Text Display alert message  Day out of valid range. Day must be between 1 and 31 (when year is 2016 and month is 12).
  Click Close Pop-up alert

TC_17 Create EDC: Month out of valid range.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Input term id  ${termid}
  Input main term id  ${termid}
  Input date of install manual  01132016
  Click Save Button on create page
  Text Display alert message  Month out of valid range. Month must be between 1 and 12.
  Click Close Pop-up alert

TC_18 Create EDC: Year out of valid range.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input mer name on qr  ${merqrname}
  Input term id  ${termid}
  Input main term id  ${termid}
  Input date of install manual  011122564
  Click Save Button on create page
  Text Display alert message  Year out of valid range. Year must be between 1970 and 2037.
  Click Close Pop-up alert

TC_19 Create EDC: Merchant Name on QR must be specified.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  3
  Input term id  ${termid}
  Input main term id  ${termid}
  Input date of install
  Click Save Button on create page
  Text Display alert message  Merchant Name on QR must be specified.
  Click Close Pop-up alert

TC_20 Create Static QR: Terminal No. must be specified.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  3
  Click Save Button on create page
  Text Display alert message  Terminal No. must be specified.
  Click Close Pop-up alert

TC_21 Create Static QR: Terminal No. must be 8 digits.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  3
  Input term id  1111
  Click Save Button on create page
  Text Display alert message  Terminal No. must be 8 digits.
  Click Close Pop-up alert

TC_22 Create Static QR: Terminal No. invalid format
  [Tags]    present4
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  3
  Input term id  11111เทส
  Click Save Button on create page
  Text Display alert message  Terminal No. can contain only English letters and numbers.
  Click Close Pop-up alert

TC_23 Create Static QR: Terminal No. with Eng. letters
  [Tags]    success
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  3
  Input term id  aaaabbbb
  Select Yes original static qr
  Input mer name on qr Static QR   ${merqrname}
  Input date of install Static QR
  Click Save Button on create page
  Click Yes Pop-up Confirm
  Text Display If merchant is General, Please specify telegram
  Click Save button after create
  Click Yes Pop-up Confirm
  Text Display Saved successfully
  Search termid static qr
  Click Search Button
  Click to open terminal
  Click Delete Button
  Confirm to Delete   ${termid}
  Text Display Deleted successfully   ${termid}
  Search termid static qr
  Click Search Button
  Text Display Data is not found

TC_24 Create Static QR: Terminal No. already exist.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  3
  Input term id  20001251
  Click Save Button on create page
  Text Display alert message  Terminal No. already exist.
  Click Close Pop-up alert

TC_25 Create Static QR: Original static QR must be selected.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  3
  Input term id  ${termid}
  Click Save Button on create page
  Text Display alert message  Original static QR must be selected.
  Click Close Pop-up alert

TC_26 Create Static QR: Merchant Name on QR must be specified.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  3
  Input term id  ${termid}
  Select Yes original static qr
  Click Save Button on create page
  Text Display alert message  Merchant Name on QR must be specified.
  Click Close Pop-up alert

TC_27 Create Static QR: Date of installation must be specified.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  3
  Input term id  ${termid}
  Select Yes original static qr
  Input row mer name on qr  ${merqrname}
  Click Save Button on create page
  Text Display alert message  Date of installation must be specified.
  Click Close Pop-up alert

TC_28 Create E-commerce: Terminal No. must be specified.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  4
  Click Save Button on create page
  Text Display alert message  Terminal No. must be specified.
  Click Close Pop-up alert

TC_29 Create E-commerce: Terminal No. must be 8 digits.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  4
  Input term id  1111
  Click Save Button on create page
  Text Display alert message  Terminal No. must be 8 digits.
  Click Close Pop-up alert

TC_30 Create E-commerce: Terminal No. with Eng. letters
  [Tags]    success
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  4
  Input term id  ccccdddd
  Input date of install
  Click Save Button on create page
  Click Yes Pop-up Confirm
  Text Display Saved successfully
  Search termid e-comm
  Click Search Button
  Click to open terminal
  Click Delete Button
  Confirm to Delete   ${termid}
  Text Display Deleted successfully   ${termid}
  Search termid e-comm
  Click Search Button
  Text Display Data is not found

TC_31 Create E-commerce: Terminal No. can contain only English letters and numbers.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  4
  Input term id  aaaabเทส
  Click Save Button on create page
  Text Display alert message  Terminal No. can contain only English letters and numbers.
  Click Close Pop-up alert

TC_32 Create E-commerce: Terminal No. already exist.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  4
  Input term id  20001251
  Click Save Button on create page
  Text Display alert message  Terminal No. already exist.
  Click Close Pop-up alert

TC_33 Create E-commerce: Date of installation must be specified.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  4
  Input term id  ${termid}
  Click Save Button on create page
  Text Display alert message  Date of installation must be specified.
  Click Close Pop-up alert

TC_34 Create EDC: Successfully B Scan C
  [Tags]    success
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  2
  Input term id  ${termid}
  Input main term id  ${termid}
  Input date of install
  Select allow refund  -1
  Click Save Button on create page
  Click Yes Pop-up Confirm
  Text Display Saved successfully
  Search termid edc
  Click Search Button
  Click to open terminal
  Checking value edc B scan C
  Click Delete Button
  Confirm to Delete   ${termid}
  Text Display Deleted successfully   ${termid}

TC_35 Create EDC: Successfully C Scan B
  [Tags]    success
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  1
  Input term id  ${termid}
  Input main term id  ${termid}
  Input date of install
  Select allow refund  -1
  Input mer name on qr  ${merqrname}
  Click Save Button on create page
  Click Yes Pop-up Confirm
  Text Display Saved successfully
  Search termid edc
  Click Search Button
  Click to open terminal
  Checking value edc
  Click Delete Button
  Confirm to Delete   ${termid}
  Text Display Deleted successfully   ${termid}

TC_36 Create EDC: Successfully refund not allowed
  [Tags]    success
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  1
  Input term id  ${termid}
  Input main term id  ${termid}
  Input date of install
  Click for exit focus
  Select allow refund  0
  Input mer name on qr  ${merqrname}
  Click Save Button on create page
  Click Yes Pop-up Confirm
  Text Display Saved successfully
  Search termid edc
  Click Search Button
  Click to open terminal
  Checking value edc
  Click Delete Button
  Confirm to Delete   ${termid}
  Text Display Deleted successfully   ${termid}

TC_37 Create EDC: Successfully Single refund
  [Tags]    success
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  1
  Input term id  ${termid}
  Input main term id  ${termid}
  Input date of install
  Click for exit focus
  Select allow refund  1
  Input mer name on qr  ${merqrname}
  Click Save Button on create page
  Click Yes Pop-up Confirm
  Text Display Saved successfully
  Search termid edc
  Click Search Button
  Click to open terminal
  Checking value edc
  Click Delete Button
  Confirm to Delete   ${termid}
  Text Display Deleted successfully   ${termid}

TC_38 Create EDC: Successfully Multiple refunds
  [Tags]    success
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  1
  Select payment mode  1
  Input term id  ${termid}
  Input main term id  ${termid}
  Input date of install
  Select allow refund  2
  Input mer name on qr  ${merqrname}
  Click Save Button on create page
  Click Yes Pop-up Confirm
  Text Display Saved successfully
  Search termid edc
  Click Search Button
  Click to open terminal
  Checking value edc
  Click Delete Button
  Confirm to Delete   ${termid}
  Text Display Deleted successfully   ${termid}

TC_39 Create Static QR: (original qr = No) Please specify Cashier ID, Product, Amount at least one.
  [Tags]    failure
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  3
  Input term id  ${termid}
  Select No original static qr
  Click Save Button on create page
  Text Display alert message  Please specify Cashier ID, Product, Amount at least one.
  Click Close Pop-up alert

TC_40 Reset and Close button on create page
  [Tags]    present3
  Generate numeric   8
  Generate Merchant Name
  Click Create
  Input mer id   ${merid}
  Select device type  4
  Input term id  ${termid}
  Input date of install
  Sleep   1.5s
  Click Reset Button on create page
  Sleep   1.5s
  Click Yes Pop-up Confirm
  Sleep   2s
  Checking reset create
  Click Close Button on create page
  Click Yes Pop-up Confirm
  Display main menu page
  Sleep   2s

*** Keywords ***
Start Testing
  Go to    ${URL}
  Go to Terminal

Go to Terminal
  Page Should Contain Link  link=Main Data
  Click Link  link=Main Data
  Click Link  link=Terminals

Click Create
  Page Should Contain Link  link=Create
  Click Link  link=Create
  Page Should Contain  Terminal

Display main menu page
  Page Should Contain  Search
  Page Should Contain  Terminal No.
  Page Should Contain  Main Terminal ID.

## InputData ##
Input mer id
  [Arguments]  ${merid}
  Input Text  id=merid   ${merid}

Input term id
  [Arguments]  ${termid}
  Input Text  id=termid   ${termid}
  Set Test Variable   ${termid}

Input main term id
  [Arguments]  ${maintermid}
  Input Text  id=maintermid   ${maintermid}
  Set Test Variable   ${maintermid}

Input date of install manual
  [Arguments]  ${date}
  Input Text  id=dateInstall   ${date}

Input date of install
  Click Element   id=dateInstall
  Sleep   0.5s
  Click Element    xpath=//button[contains(@class, "ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all")][contains(text(), "Today")]

Input date of install Static QR
  Click Element   id=insdt_0
  Sleep   0.5s
  Click Element    xpath=//button[contains(@class, "ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all")][contains(text(), "Today")]

Input mer name on qr
  [Arguments]  ${mer_qr_name}
  Input Text  id=qrmername   ${mer_qr_name}
  Set Test Variable   ${mer_qr_name}

Input mer name on qr Static QR
  [Arguments]  ${mer_qr_name}
  Input Text  id=qrmername_0   ${mer_qr_name}
  Set Test Variable   ${mer_qr_name}

Input row date of install
  Click Element   id=insdt_0
  Sleep   0.5s
  Click Element    xpath=//button[contains(@class, "ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all")][contains(text(), "Today")]

Input row product
  [Arguments]   ${product}
  Input Text  id=product_0   ${product}
  Set Test Variable   ${product}

Input row amount
  [Arguments]   ${amt}
  Input Text  id=amt_0   ${amt}
  Set Test Variable   ${amt}

Input row mer name on qr
  [Arguments]  ${mer_qr_name}
  Input Text  id=qrmername_0   ${mer_qr_name}
  Set Test Variable   ${mer_qr_name}

Input cashier id
  [Arguments]  ${cashier}
  Input Text  id=cashies_0   ${cashier}
  Set Test Variable   ${cashier}

Input telegram ID
  [Arguments]  ${telegram}
  Input Text   id=tlgusername_0   ${telegram}

Input email
  [Arguments]  ${email}
  Input Text   id=email_0   ${email}

Select disable
  Select Checkbox    id=disabled_0

Search termid edc
  Input Text  id=maintermid  ${maintermid}
  Input Text  id=termid  ${termid}
  Input Text  id=merid  ${merid}

Search termid e-comm
  Input Text  id=termid  ${termid}
  Input Text  id=merid  ${merid}

Search termid static qr
  Input Text  id=termid  ${termid}
  Input Text  id=merid  ${merid}

## Click Button ##
Add row
  Click Element   //*[@id="res-form"]/div/a

Add telegram row
  Click Element   //*[@id="tlg-table"]/a/span/img

Remove row
  Click Element   //*[@id="respmsgmapslists"]/li[2]/table/tbody/tr/td[1]/a/img

Select device type
  [Arguments]    ${devtype}     #edc/qr/ecom
  Select Radio Button   model.termtype   ${devtype}
  Set Test Variable   ${devtype}

Select payment mode
  [Arguments]    ${paymode}     #CsB/BsC/All
  Select Radio Button   model.paymentmode   ${paymode}
  Set Test Variable   ${paymode}

Select allow refund
  [Arguments]    ${allowrefund}     #-1 inh/0 notallow/1 single/2 multi
  Select Radio Button   model.allowrefund   ${allowrefund}
  Set Test Variable   ${allowrefund}

Select search format
  [Arguments]    ${format}
  Click Element   xpath=//*[@id="rptFormat"]/option[${format}]

Select No original static qr
  Click Element  //*[@id="qrdf"]/option[3]

Select Yes original static qr
  Click Element  //*[@id="qrdf"]/option[2]

Click to open terminal
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain    ${termid}
  Click Image    //*[@id="content"]/table/tbody/tr[1]/td[2]/a/img
  Wait Until Keyword Succeeds    10s    2s   Page Should Contain  Terminal

Click to open telegram
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain    ${termid}
  Click Image    xpath=/html/body/div[1]/div[5]/div[2]/div/form/table[2]/tbody/tr[10]/td/div/table/tbody/tr/td[8]/a/img
  Wait Until Keyword Succeeds    10s    2s   Page Should Contain  Terminal

Click Save button after create
    Sleep   0.5s
    Click Element    xpath=/html/body/div[1]/div[5]/div[2]/div/form/table[2]/tbody/tr[16]/td[2]/input[1]

Click for exit focus
  Click Element   //*[@id="view-form"]/table[2]/tbody/tr[14]/td[1]

Pop-up display update telegram successful
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Update telegram successful
  sleep    1s   ##ถ้าไม่ sleep จะหาปุ่มไม่เจอ
  Click Close Pop-up alert

Confirm to Delete
  [Arguments]   ${termid}
  Wait Until Keyword Succeeds   10s  2s   Page Should Contain  Do you want to delete Terminal ${termid} ?
  sleep    1s   ##ถ้าไม่ sleep จะหายปุ่มไม่เจอ
  Click Yes Pop-up Confirm

## Display ##
Text Display If merchant is General, Please specify telegram
  Wait Until Page Contains   If merchant is General, Please specify telegram.   5s
  Sleep   2s

Text Display Deleted successfully
  [Arguments]   ${mer_id}
  Wait Until Page Contains   Terminal ${termid} has been deleted.   5s
  Sleep   2s

## Checking ##
Checking value edc
  Textfield Value Should Be   id=merid   ${merid}
  Page Should Contain  EDC
  Radio Button Should Be Set To    model.paymentmode   ${paymode}
  Page Should Contain  ${termid}
  Textfield Value Should Be   id=maintermid   ${termid}
  Radio Button Should Be Set To    model.allowrefund   ${allowrefund}
  Textfield Value Should Be   id=qrmername   ${merqrname}
  Radio Button Should Be Set To    model.termstatus   A

Checking value edc B scan C
  Textfield Value Should Be   id=merid   ${merid}
  Page Should Contain  EDC
  Radio Button Should Be Set To    model.paymentmode   ${paymode}
  Page Should Contain  ${termid}
  Textfield Value Should Be   id=maintermid   ${termid}
  Radio Button Should Be Set To    model.allowrefund   ${allowrefund}
  Radio Button Should Be Set To    model.termstatus   A

Checking value e-comm
  Textfield Value Should Be   id=merid   ${merid}
  Page Should Contain  E-commerce
  Page Should Contain  ${termid}
  Radio Button Should Be Set To    model.termstatus   A

Checking value static qr
  Textfield Value Should Be   id=merid   ${merid}
  Page Should Contain  Static QR
  Page Should Contain  ${termid}
  Textfield Value Should Be   name=model.terminfo[0].cashies   ${cashier}
  Textfield Value Should Be   name=model.terminfo[0].product   ${product}
  Textfield Value Should Be   name=model.terminfo[0].amt   ${amt}
  Textfield Value Should Be   name=model.terminfo[0].qrmername   ${merqrname}
  Radio Button Should Be Set To    model.termstatus   A

Checking reset create
  Textfield Value Should Be   id=merid   \
  Radio Button Should Be Set To    model.termtype   1
  Radio Button Should Be Set To    model.paymentmode   3
  Textfield Value Should Be   id=termid   \
  Textfield Value Should Be   id=maintermid   \
  Textfield Value Should Be   model.insdt   \
  Radio Button Should Be Set To    model.allowrefund   -1
  Textfield Value Should Be   id=qrmername   \

Checking reset search
  Textfield Value Should Be   id=resname   \
  List Selection Should Be   id=rptFormat   vh

## Generating ##
Generate numeric
    [Arguments]  ${len}
    [Documentation]  Generate numeric
    ${termid}    Generate random string    ${len}   1234567890
    Set Test Variable   ${termid}

Generate Merchant Name
    [Documentation]  Generate Merchant Name
    ${mer_qr_name}   Set Variable   QR_&^?><'!${termid}
    Set Test Variable   ${merqrname}

Generate Test resp msg mapping name len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890
    ${respmsgmapp}   Set Variable   TestRespMsgMapp${random_id}
    Set Test Variable   ${respmsgmapp}

Generate Test host resp msg row0 len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=-)(*&^%$#@!_'\/?><.,;:[]}{
    ${hostrespmsg_0}   Set Variable   H_${random_id}
    Set Test Variable   ${hostrespmsg_0}

Generate Test return resp msg row0 len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=-)(*&^%$#@!+_'\/?><.,;:[]}{
    ${returnrespmsg_0}   Set Variable   C_${random_id}
    Set Test Variable   ${returnrespmsg_0}

Generate Test host resp msg row1 len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=-)(*&^%$#@!+_'\/?><.,;:[]}{
    ${hostrespmsg_1}   Set Variable   H_${random_id}
    Set Test Variable   ${hostrespmsg_1}


Generate Test return resp msg row1 len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=-)(*&^%$#@!+_'\/?><.,;:[]}{
    ${returnrespmsg_1}   Set Variable   C_${random_id}
    Set Test Variable   ${returnrespmsg_1}

Generate Test host resp msg row2 len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=-)(*&^%$#@!+_'\/?><.,;:[]}{
    ${hostrespmsg_2}   Set Variable   H_${random_id}
    Set Test Variable   ${hostrespmsg_2}


Generate Test return resp msg row2 len=
    [Arguments]  ${len}
    [Documentation]  Generate Test resp msg mapping name
    ${random_id}    Generate random string    ${len}   1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ=-)(*&^%$#@!+_'\/?><.,;:[]}{
    ${returnrespmsg_2}   Set Variable   C_${random_id}
    Set Test Variable   ${returnrespmsg_2}

## Getting Value ##
Get response message mapping name from search result
  Sleep   1s
  ${name}   Get Value   xpath=//*[@id="result"]/tbody/tr[4]/td[2]
  Set Test Variable   ${name}
